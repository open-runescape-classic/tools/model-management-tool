module modeltool {
    requires javafx.fxml;
    requires javafx.controls;
    requires apachecommons;
    requires com.jfoenix;
    requires org.controlsfx.controls;
    exports com.OpenRSC;
    exports com.OpenRSC.Interface.ModelTool;
    opens com.OpenRSC.Interface.ModelTool;
}