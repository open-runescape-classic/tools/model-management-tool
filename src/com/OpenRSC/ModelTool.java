package com.OpenRSC;

import com.OpenRSC.IO.archive.Unpacker;
import com.OpenRSC.IO.texture.Entry;
import com.OpenRSC.IO.texture.Frame;
import com.OpenRSC.Render.ModelRenderer;
import com.OpenRSC.Render.client.RSModel;
import com.OpenRSC.Render.client.Scanline;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.*;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class ModelTool extends Application {

    private RSModel[] modelCache;
    private Parent mainRoot;
    private com.OpenRSC.Interface.ModelTool.Controller mainController;
    private ModelRenderer modelRenderer;
    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        setPrimaryStage(primaryStage);

        try { spinMain(); } catch (IOException a) { a.printStackTrace(); return; }

        modelRenderer = new ModelRenderer(this, mainController.getViewer());

        primaryStage.setOnCloseRequest(e -> modelRenderer.stop());
        primaryStage.setScene(new Scene(mainRoot, 1080, 720));
        primaryStage.show();

        Unpacker unpacker = new Unpacker();
        modelCache = unpacker.unpack();

        mainController.createBindings();
        mainController.listModels(modelCache);
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void spinMain() throws IOException {
        URL mainURL = new File("src/com/OpenRSC/Interface/ModelTool/form.fxml").toURI().toURL();
        FXMLLoader mainLoader = new FXMLLoader(mainURL);
        this.mainRoot = mainLoader.load();
        this.mainController = mainLoader.getController();
        this.mainController.setModelTool(this);
    }

    public ModelRenderer getModelRenderer() { return this.modelRenderer; }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public com.OpenRSC.Interface.ModelTool.Controller getMainController() { return this.mainController; }

    public void addAvailableTexture(Entry entry) {
        TextureEntry textureEntry = new TextureEntry(entry);
        getMainController().addAvailableTexture(textureEntry);
    }

    public class TextureEntry{
        String name;
        ImageView preview;
        private TextureEntry() {}
        TextureEntry(Entry entry) {
            Frame texture = entry.getFrames()[0];

            //Add alpha to the texture
            for (int i=0; i<texture.getPixels().length; ++i)
                texture.getPixels()[i] |= 0xFF000000;

            WritableImage write = new WritableImage(texture.getBoundWidth(), texture.getBoundHeight());
            PixelWriter pw = write.getPixelWriter();
            pw.setPixels(0, 0, texture.getBoundWidth(), texture.getBoundHeight(), PixelFormat.getIntArgbInstance(), texture.getPixels(), 0, texture.getBoundWidth());
            preview = new ImageView(write);
            preview.setPreserveRatio(true);
            preview.setFitWidth(32);
            this.name = entry.getID();
        }

        public ImageView getPreview() { return this.preview; }
        public String getName() { return name; }

        public TextureEntry clone() {
            TextureEntry ret = new TextureEntry();
            ret.name = new String(this.name);
            ret.preview = new ImageView(this.preview.getImage());
            ret.preview.setPreserveRatio(true);
            ret.preview.setFitWidth(32);
            return ret;
        }
    }
}
