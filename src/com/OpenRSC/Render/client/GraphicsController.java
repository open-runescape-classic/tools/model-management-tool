package com.OpenRSC.Render.client;

import com.OpenRSC.Render.client.utils.FastMath;
import com.OpenRSC.Render.client.utils.GenUtil;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.zip.ZipFile;

public class GraphicsController {

	public enum SPRITE_LAYER {
		MINIMAP, WORLDMAP, SHOP
	}

	public boolean interlace = false;
	public boolean loggedIn = false;
	public int height2;
	public volatile int[] pixelData; // TODO: Changed to volatile in order for rendering and javafx window thread to both be able to access this
	public int width2;
	public Sprite[] sprites;
	public Sprite[] spriteVerts = new Sprite[3];
	public Sprite minimapSprite = new Sprite();
	private int clipTop = 0;
	private int iconSpriteIndex;
	private int clipLeft = 0;
	private int[] trigTable256;
	private int clipRight = 0;
	private int[] m_M;
	private int clipBottom = 0;
	private int[] m_t;
	private int[] m_tb;
	private int[] m_Tb;
	private int[] m_Wb;
	//public Map<String, Map<String, Entry>> spriteTree = new HashMap<>();
	// public int[][] image2D_pixels;
	private int[] m_Xb;
	private ZipFile spriteArchive;

	public GraphicsController(int var1, int var2) {
		try {
			this.clipBottom = var2;
			this.clipRight = var1;
			this.pixelData = new int[var1 * var2];
			this.height2 = var2;
			this.width2 = var1;
		} catch (RuntimeException var7) {
			throw GenUtil.makeThrowable(var7, "ua.<init>(" + var1 + ',' + var2 + ',' + ')' + ')');
		}
	}

	public static void a(int var0, int[] var1, int var2, int[] var3, int var4, int var5, int var6, int var7) {
		try {

			if (var2 < 0) {
				var4 = var1[('\uffa7' & var0) >> 8];
				var5 <<= 2;
				var0 += var5;
				int var8 = var2 / 16;

				int var9;
				for (var9 = var8; var9 < 0; ++var9) {
					var3[var6++] = var4 + FastMath.bitwiseAnd(0x7F7F7F, var3[var6] >> 1);
					var3[var6++] = var4 + FastMath.bitwiseAnd(0x7F7F7F, var3[var6] >> 1);
					var3[var6++] = var4 + (FastMath.bitwiseAnd(var3[var6], 0xFEFEFF) >> 1);
					var3[var6++] = var4 + (FastMath.bitwiseAnd(0xFEFEFF, var3[var6]) >> 1);
					var4 = var1[255 & var0 >> 8];
					var0 += var5;
					var3[var6++] = FastMath.bitwiseAnd(var3[var6] >> 1, 0x7F7F7F) + var4;
					var3[var6++] = FastMath.bitwiseAnd(var3[var6] >> 1, 0x7F7F7F) + var4;
					var3[var6++] = (FastMath.bitwiseAnd(0xFEFEFF, var3[var6]) >> 1) + var4;
					var3[var6++] = FastMath.bitwiseAnd(var3[var6] >> 1, 0x7F7F7F) + var4;
					var4 = var1[var0 >> 8 & 255];
					var3[var6++] = (FastMath.bitwiseAnd(var3[var6], 0xFEFEFE) >> 1) + var4;
					var0 += var5;
					var3[var6++] = (FastMath.bitwiseAnd(0xFEFEFF, var3[var6]) >> 1) + var4;
					var3[var6++] = var4 + FastMath.bitwiseAnd(var3[var6] >> 1, 0x7F7F7F);
					var3[var6++] = var4 + FastMath.bitwiseAnd(0x7F7F7F, var3[var6] >> 1);
					var4 = var1[('\uff16' & var0) >> 8];
					var0 += var5;
					var3[var6++] = (FastMath.bitwiseAnd(var3[var6], 0xFEFEFF) >> 1) + var4;
					var3[var6++] = var4 + FastMath.bitwiseAnd(var3[var6] >> 1, 0x7F7F7F);
					var3[var6++] = var4 + (FastMath.bitwiseAnd(var3[var6], 0xFEFEFF) >> 1);
					var3[var6++] = var4 + (FastMath.bitwiseAnd(0xFEFEFF, var3[var6]) >> 1);
					var4 = var1[var0 >> 8 & 255];
					var0 += var5;
				}

				var8 = -(var2 % 16);

				for (var9 = var7; var8 > var9; ++var9) {
					var3[var6++] = FastMath.bitwiseAnd(var3[var6] >> 1, 0x7F7F7F) + var4;
					if ((3 & var9) == 3) {
						var4 = var1[(var0 & '\uff38') >> 8];
						var0 += var5;
						var0 += var5;
					}
				}

			}
		} catch (RuntimeException var10) {
			throw GenUtil.makeThrowable(var10,
					"ua.S(" + var0 + ',' + (var1 != null ? "{...}" : "null") + ',' + var2 + ','
							+ (var3 != null ? "{...}" : "null") + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7
							+ ')');
		}
	}

	public void resize(int width, int height) {
		this.clipBottom = height;
		this.clipRight = width;
		this.pixelData = new int[width * height];
		this.height2 = height;
		this.width2 = width;
	}

	public final void clearClip() {
		try {
			this.clipRight = this.width2;
			this.clipLeft = 0;
			this.clipTop = 0;
			this.clipBottom = this.height2;

		} catch (RuntimeException var3) {
			throw GenUtil.makeThrowable(var3, "ua.JA(" + "dummy" + ')');
		}
	}

	public final void spriteClipping(Sprite sprite, byte var2, int height, int var4, int width, int var6, int var7) {
		try {


			try {
				int spriteWidth = sprite.getWidth();// this.image2D_width[sprite];
				int spriteHeight = sprite.getHeight();
				int var10 = 0;
				int var11 = 0;
				int scaleX = (spriteWidth << 16) / width;
				int scaleY = (spriteHeight << 16) / height;
				if (sprite.requiresShift()) {
					int var14 = sprite.getSomething1();
					int var15 = sprite.getSomething2();
					if (var14 == 0 || var15 == 0) {
						return;
					}

					scaleY = (var15 << 16) / height;
					var6 += (var15 + height * sprite.getYShift() - 1) / var15;
					var4 += (var14 + sprite.getXShift() * width - 1) / var14;
					scaleX = (var14 << 16) / width;
					if (width * sprite.getXShift() % var14 != 0) {
						var10 = (var14 - sprite.getXShift() * width % var14 << 16) / width;
					}

					if (sprite.getYShift() * height % var15 != 0) {
						var11 = (var15 - sprite.getYShift() * height % var15 << 16) / height;
					}

					width = width * (sprite.getWidth() - (var10 >> 16)) / var14;
					height = (sprite.getHeight() - (var11 >> 16)) * height / var15;
				}

				int var14 = var6 * this.width2 + var4;
				if (var2 > -121) {
					return;
				}

				int var16;
				if (this.clipTop > var6) {
					var16 = this.clipTop - var6;
					height -= var16;
					var6 = 0;
					var14 += this.width2 * var16;
					var11 += scaleY * var16;
				}

				int var15 = this.width2 - width;
				if (var4 < this.clipLeft) {
					var16 = this.clipLeft - var4;
					var4 = 0;
					var10 += var16 * scaleX;
					var14 += var16;
					width -= var16;
					var15 += var16;
				}

				if (var6 + height >= this.clipBottom) {
					height -= 1 + height + (var6 - this.clipBottom);
				}

				if (var4 + width >= this.clipRight) {
					var16 = 1 + var4 + (width - this.clipRight);
					var15 += var16;
					width -= var16;
				}

				byte var19 = 1;
				if (this.interlace) {
					scaleY += scaleY;
					var15 += this.width2;
					if ((var6 & 1) != 0) {
						var14 += this.width2;
						--height;
					}

					var19 = 2;
				}

				this.plot_tran_scale(var19, var11, width, (byte) -61, scaleY, spriteWidth, scaleX, height, var14,
						sprite.getPixels(), 0, var10, var15, var7, this.pixelData);
			} catch (Exception var17) {
				System.out.println("error in sprite clipping routine");
			}

		} catch (RuntimeException var18) {
			throw GenUtil.makeThrowable(var18, "ua.E(" + sprite + ',' + var2 + ',' + height + ',' + var4 + ',' + width
					+ ',' + var6 + ',' + var7 + ')');
		}
	}

	public final void a(Sprite sprite, int var2, int var3, int var4, int var5) {
		try {
			if (sprite == null) {
				System.out.println("Sprite missing: " + sprite.getID());
				return;
			}
			if (sprite.requiresShift()) {
				var3 += sprite.getXShift();
				var5 += sprite.getYShift();
			}


			int var6 = this.width2 * var5 + var3;
			int var7 = var2;
			int var8 = sprite.getHeight();
			int var9 = sprite.getWidth();
			int var10 = this.width2 - var9;
			int var11 = 0;
			int var12;
			if (var5 < this.clipTop) {
				var12 = this.clipTop - var5;
				var5 = this.clipTop;
				var8 -= var12;
				var7 = var2 + var12 * var9;
				var6 += var12 * this.width2;
			}

			if (var8 + var5 >= this.clipBottom) {
				var8 -= 1 + var8 + (var5 - this.clipBottom);
			}

			if (var3 < this.clipLeft) {
				var12 = this.clipLeft - var3;
				var10 += var12;
				var11 += var12;
				var6 += var12;
				var7 += var12;
				var3 = this.clipLeft;
				var9 -= var12;
			}

			if (this.clipRight <= var3 + var9) {
				var12 = var3 - (-var9 - 1) - this.clipRight;
				var10 += var12;
				var9 -= var12;
				var11 += var12;
			}

			if (var9 > 0 && var8 > 0) {
				byte var14 = 1;
				if (this.interlace) {
					var11 += sprite.getWidth();
					var14 = 2;
					if ((1 & var5) != 0) {
						--var8;
						var6 += this.width2;
					}

					var10 += this.width2;
				}
				// TODO:
				// if (this.image2D_pixels[sprite] == null) {
				// this.a(var7, var10, this.spriteColours[sprite], var14, false,
				// var8, var11, var4, var9, this.pixelData,
				// this.image2D_colorLookupTable[sprite], var6);
				// } else {
				this.a(var7, var8, var6, sprite.getPixels(), 0, var4, var14, this.pixelData, -107, var11,
						var10, var9);
				// }

			}
		} catch (RuntimeException var13) {
			throw GenUtil.makeThrowable(var13,
					"ua.T(" + sprite + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ')');
		}
	}

	public final void spriteClip3(int var1, int var2, Sprite sprite, int var4, int var5, byte var6, int var7) {
		try {


			try {
				int spriteWidth = sprite.getWidth();
				int spriteHeight = sprite.getHeight();
				int var10 = 0;
				int var11 = 0;
				int var12 = (spriteWidth << 16) / var7;
				int var13 = (spriteHeight << 16) / var5;
				int var14;
				int var15;
				if (sprite.requiresShift()) {
					var14 = sprite.getSomething1();
					var15 = sprite.getSomething2();
					if (var14 == 0 || var15 == 0) {
						return;
					}

					if (sprite.getXShift() * var7 % var14 != 0) {
						var10 = (var14 - sprite.getXShift() * var7 % var14 << 16) / var7;
					}

					var1 += (var7 * sprite.getXShift() + var14 - 1) / var14;
					var12 = (var14 << 16) / var7;
					var4 += (var15 + var5 * sprite.getYShift() - 1) / var15;
					var13 = (var15 << 16) / var5;
					if (sprite.getYShift() * var5 % var15 != 0) {
						var11 = (var15 - var5 * sprite.getYShift() % var15 << 16) / var5;
					}

					var5 = var5 * (sprite.getHeight() - (var11 >> 16)) / var15;
					var7 = (sprite.getWidth() - (var10 >> 16)) * var7 / var14;
				}

				var14 = var1 + var4 * this.width2;
				var15 = this.width2 - var7;
				int var16;
				if (this.clipTop > var4) {
					var16 = this.clipTop - var4;
					var4 = 0;
					var5 -= var16;
					var14 += var16 * this.width2;
					var11 += var16 * var13;
				}

				if (this.clipLeft > var1) {
					var16 = this.clipLeft - var1;
					var7 -= var16;
					var15 += var16;
					var10 += var16 * var12;
					var14 += var16;
					var1 = 0;
				}

				if (this.clipBottom <= var4 + var5) {
					var5 -= var4 + var5 - (this.clipBottom - 1);
				}

				if (this.clipRight <= var1 + var7) {
					var16 = var1 + var7 + (1 - this.clipRight);
					var7 -= var16;
					var15 += var16;
				}

				byte var19 = 1;
				if (this.interlace) {
					var13 += var13;
					if ((1 & var4) != 0) {
						--var5;
						var14 += this.width2;
					}

					var19 = 2;
					var15 += this.width2;
				}

				this.a(var11, var12, var7, var10, var15, sprite.getPixels(), var14, this.pixelData, 0,
						spriteWidth, false, var13, var5, var2, var19);
			} catch (Exception var17) {
				System.out.println("error in sprite clipping routine");
			}

		} catch (RuntimeException var18) {
			throw GenUtil.makeThrowable(var18, "ua.FA(" + var1 + ',' + var2 + ',' + sprite + ',' + var4 + ',' + var5
					+ ',' + var6 + ',' + var7 + ')');
		}
	}

	public void drawEntity(int index, int x, int y, int width, int height, int var1, int var8) {
		try {
			Sprite sprite = sprites[index];
			this.drawSprite(sprite, x, y, width, height, 5924);

		} catch (RuntimeException var10) {
			throw GenUtil.makeThrowable(var10, "ua.B(" + var1 + ',' + index + ',' + height + ',' + x + ',' + y + ','
					+ width + ',' + 29 + ',' + var8 + ')');
		}
	}

	public final void a(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
		try {
			for (int var8 = var4; var8 < var4 + var6; ++var8) {
				for (int var9 = var3; var3 + var1 > var9; ++var9) {
					int var10 = 0;
					int var11 = 0;
					int var12 = 0;
					int var13 = 0;

					for (int var14 = var8 - var7; var14 <= var7 + var8; ++var14) {
						if (var14 >= 0 && this.width2 > var14) {
							for (int var15 = var9 - var2; var15 <= var9 + var2; ++var15) {
								if (var15 >= 0 && this.height2 > var15) {
									int inLetter = this.pixelData[this.width2 * var15 + var14];
									var12 += 0xFF & inLetter;
									++var13;
									var11 += (inLetter & 0xFF81) >> 8;
									var10 += (inLetter & 0xFF64A6) >> 16;
								}
							}
						}
					}

					this.pixelData[var8 + var9 * this.width2] = var12 / var13 + (var10 / var13 << 16)
							+ (var11 / var13 << 8);
				}
			}


		} catch (RuntimeException var17) {
			throw GenUtil.makeThrowable(var17, "ua.VA(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + 16740352
					+ ',' + var6 + ',' + var7 + ')');
		}
	}

	private void a(int var1, int var2, int var3, int var4, int var5, int[] var6, int var7, int[] letterPlotTable,
				   int var9, int var10, boolean var11, int var12, int var13, int var14, int var15) {
		try {

			int var16 = (var14 & 16736117) >> 16;
			int var17 = 255 & var14 >> 8;
			int var18 = 255 & var14;

			try {
				int var19 = var4;
				if (var11) {
					this.m_Tb = (int[]) null;
				}

				for (int var20 = -var13; var20 < 0; var20 += var15) {
					int var21 = (var1 >> 16) * var10;

					for (int var22 = -var3; var22 < 0; ++var22) {
						var9 = var6[var21 + (var4 >> 16)];
						if (var9 != 0) {
							int var23 = var9 >> 16 & 255;
							int var24 = ('\uff60' & var9) >> 8;
							int var25 = 255 & var9;
							if (var23 == var24 && var25 == var24) {
								letterPlotTable[var7++] = (var23 * var16 >> 8 << 16) + (var17 * var24 >> 8 << 8)
										+ (var25 * var18 >> 8);
							} else {
								letterPlotTable[var7++] = var9;
							}
						} else {
							++var7;
						}

						var4 += var2;
					}

					var1 += var12;
					var7 += var5;
					var4 = var19;
				}
			} catch (Exception var26) {
				System.out.println("error in plot_scale");
			}

		} catch (RuntimeException var27) {
			throw GenUtil.makeThrowable(var27,
					"ua.EB(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ','
							+ (var6 != null ? "{...}" : "null") + ',' + var7 + ','
							+ (letterPlotTable != null ? "{...}" : "null") + ',' + var9 + ',' + var10 + ',' + var11
							+ ',' + var12 + ',' + var13 + ',' + var14 + ',' + var15 + ')');
		}
	}

	/**
	 * @param negCount negated output pixel count
	 * @param srcWidth source data width
	 * @param dest     destination data
	 * @param srcStepX (source x per output pixel) << 17
	 * @param srcHeadY (start source y) << 17
	 * @param srcHeadX (start source x) << 17
	 * @param src      source pixel data
	 * @param srcStepY (source y per output pixel) << 17
	 * @param destHead starting pixel address in pixelData
	 */
	private void plot_horiz_line(int negCount, int srcWidth, int[] dest, int srcStepX, int srcHeadY, int srcHeadX,
								 int[] src, int srcStepY, int destHead) {
		try {
			for (int i = negCount; i < 0; ++i) {
				dest[destHead++] = src[(srcHeadY >> 17) * srcWidth + (srcHeadX >> 17)];
				srcHeadX += srcStepX;
				srcHeadY += srcStepY;
			}

		} catch (RuntimeException var13) {
			throw GenUtil.makeThrowable(var13,
					"ua.M(" + negCount + ',' + srcWidth + ',' + "dummy" + ',' + (dest != null ? "{...}" : "null") + ','
							+ srcStepX + ',' + srcHeadY + ',' + srcHeadX + ',' + (src != null ? "{...}" : "null") + ','
							+ srcStepY + ',' + destHead + ',' + "dummy" + ')');
		}
	}

	private void a(int var1, int var2, int var3, int[] var4, int var5, int var6, int var7, int[] var8, int var9,
				   int var10, int var11, int var12) {
		try {

			int var13 = 256 - var6;
			if (var9 <= -54) {
				for (int var14 = -var2; var14 < 0; var14 += var7) {
					for (int var15 = -var12; var15 < 0; ++var15) {
						var5 = var4[var1++];
						if (var5 == 0) {
							++var3;
						} else {
							int var16 = var8[var3];
							var8[var3++] = FastMath.bitwiseAnd(16711680,
									var6 * FastMath.bitwiseAnd(var5, '\uff00')
											+ var13 * FastMath.bitwiseAnd('\uff00', var16))
									+ FastMath.bitwiseAnd(var13 * FastMath.bitwiseAnd(var16, 16711935)
									+ FastMath.bitwiseAnd(var5, 16711935) * var6, -16711936) >> 8;
						}
					}

					var1 += var10;
					var3 += var11;
				}

			}
		} catch (RuntimeException var17) {
			throw GenUtil.makeThrowable(var17,
					"ua.TA(" + var1 + ',' + var2 + ',' + var3 + ',' + (var4 != null ? "{...}" : "null") + ',' + var5
							+ ',' + var6 + ',' + var7 + ',' + (var8 != null ? "{...}" : "null") + ',' + var9 + ','
							+ var10 + ',' + var11 + ',' + var12 + ')');
		}
	}

	/**
	 * @param negCount negated output pixel count
	 * @param srcWidth source data width
	 * @param dest     destination data
	 * @param srcStepX (source x per output pixel) << 17
	 * @param srcHeadY (start source y) << 17
	 * @param srcHeadX (start source x) << 17
	 * @param src      source pixel data
	 * @param srcStepY (source y per output pixel) << 17
	 * @param destHead starting pixel address in pixelData
	 */
	private void plot_trans_horiz_line(int srcStepY, int negCount, int srcHeadX, int[] src, int[] var5,
									   int srcHeadY, int destHead, int srcWidth, int srcStepX) {
		try {

			int i = negCount;
			while (i < 0) {
				int color = src[(srcHeadY >> 17) * srcWidth + (srcHeadX >> 17)];
				// System.out.println("color: " + color);
				if (color == 0) {
					// System.out.println("color is black");
					++destHead;
				} else {
					var5[destHead++] = color;
				}

				srcHeadX += srcStepX;
				srcHeadY += srcStepY;
				++i;
			}
		} catch (RuntimeException var13) {
			throw GenUtil.makeThrowable(var13,
					"ua.UA(" + srcStepY + ',' + negCount + ',' + srcHeadX + ',' + (src != null ? "{...}" : "null") + ','
							+ (var5 != null ? "{...}" : "null") + ',' + srcHeadY + ',' + destHead + ',' + srcWidth + ','
							+ srcStepX + ',' + "dummy" + ',' + "dummy" + ')');
		}
	}

    public final void setClip(int clipLeft, int clipRight, int clipBottom, int clipTop) {
        try {
            if (this.height2 < clipBottom) {
                clipBottom = this.height2;
            }

            if (clipTop < 0) {
                clipTop = 0;
            }

            if (clipLeft < 0) {
                clipLeft = 0;
            }


            if (clipRight > this.width2) {
                clipRight = this.width2;
            }

            this.clipTop = clipTop;
            this.clipBottom = clipBottom;
            this.clipRight = clipRight;
            this.clipLeft = clipLeft;
        } catch (RuntimeException var7) {
            throw GenUtil.makeThrowable(var7,
                    "ua.NA(" + clipLeft + ',' + clipRight + ',' + clipBottom + ',' + clipTop + ',' + "dummy" + ')');
        }
    }

    public final void blackScreen(boolean var1) {
        try {

            int var2 = this.height2 * this.width2;
            if (var1 == !this.interlace) {
                for (int i = 0; var2 > i; ++i) {
                    this.pixelData[i] = 0xFF000001;
                }
            } else {
                int head = 0;

                for (int i = -this.height2; i < 0; i += 2) {
                    for (int j = -this.width2; j < 0; ++j) {
                        this.pixelData[head++] = 0;
                    }

                    head += this.width2;
                }
            }

        } catch (RuntimeException var6) {
            throw GenUtil.makeThrowable(var6, "ua.H(" + var1 + ')');
        }
    }
	private void a(int var1, int[] var2, int var3, int var4, int var5, int var6, byte var7, int var8, int[] var9,
				   int var10, int var11) {
		try {

			if (var7 <= 122) {
				this.drawBoxBorder(121, 54, -117, 67, -103);
			}

			int var12 = -(var1 >> 2);
			var1 = -(3 & var1);

			for (int var13 = -var4; var13 < 0; var13 += var3) {
				int var14;
				for (var14 = var12; var14 < 0; ++var14) {
					var5 = var9[var6++];
					if (var5 == 0) {
						++var8;
					} else {
						var2[var8++] = var5;
					}

					var5 = var9[var6++];
					if (var5 == 0) {
						++var8;
					} else {
						var2[var8++] = var5;
					}

					var5 = var9[var6++];
					if (var5 != 0) {
						var2[var8++] = var5;
					} else {
						++var8;
					}

					var5 = var9[var6++];
					if (var5 == 0) {
						++var8;
					} else {
						var2[var8++] = var5;
					}
				}

				for (var14 = var1; var14 < 0; ++var14) {
					var5 = var9[var6++];
					if (var5 == 0) {
						++var8;
					} else {
						var2[var8++] = var5;
					}
				}

				var6 += var11;
				var8 += var10;
			}

		} catch (RuntimeException var15) {
			throw GenUtil.makeThrowable(var15,
					"ua.JB(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ',' + var4 + ',' + var5
							+ ',' + var6 + ',' + var7 + ',' + var8 + ',' + (var9 != null ? "{...}" : "null") + ','
							+ var10 + ',' + var11 + ')');
		}
	}

	/**
	 * destHeight is height / heightStep. Black is transparent.
	 *
	 * @param heightStep    destinationHeight / height
	 * @param srcStartY     First source row << 16
	 * @param destWidth     Destination width
	 * @param dummy1
	 * @param dummy2
	 * @param scaleY        (source rows per destination row) << 16
	 * @param spriteWidth   columns in source data
	 * @param scaleX        (source columns per destination column) << 16
	 * @param height        destination height * heightStep
	 * @param destHead      first destination pixel to output to
	 * @param src           source pixel data
	 * @param srcStartX     First source column << 16
	 * @param destRowStride Pixels to skip between rows of output
	 * @param dest          Destination pixel data
	 */
	private void plot_scale_black_mask(int[] src, int heightStep, int scaleX, int dummy1, int srcStartY,
									   int[] dest, byte dummy2, int scaleY, int destHeight, int srcStartX, int destRowStride, int destWidth,
									   int srcWidth, int destHead) {
		try {


			try {
				int firstColumn = srcStartX;

				for (int i = -destHeight; i < 0; i += heightStep) {
					int srcRowHead = (srcStartY >> 16) * srcWidth;
					srcStartY += scaleY;

					for (int j = -destWidth; j < 0; ++j) {
						int color = src[(srcStartX >> 16) + srcRowHead];
						srcStartX += scaleX;
						if (color != 0) {
							dest[destHead++] = color;
						} else {
							++destHead;
						}
					}

					destHead += destRowStride;
					srcStartX = firstColumn;
				}
			} catch (Exception var20) {
				System.out.println("error in plot_scale");
			}

		} catch (RuntimeException var21) {
			throw GenUtil.makeThrowable(var21,
					"ua.DA(" + (src != null ? "{...}" : "null") + ',' + heightStep + ',' + scaleX + ',' + dummy1 + ','
							+ srcStartY + ',' + (dest != null ? "{...}" : "null") + ',' + dummy2 + ',' + scaleY + ','
							+ destHeight + ',' + srcStartX + ',' + destRowStride + ',' + destWidth + ',' + srcWidth
							+ ',' + destHead + ')');
		}
	}

	/*
	 * public final void applyColorLookupTable(int imgID) { try {
	 *  if (null != this.spriteColours[imgID]) { int
	 * pixCount = this.image2D_width[imgID] * this.image2D_height[imgID]; byte[]
	 * pixels = this.spriteColours[imgID]; int[] colorLookupTable =
	 * this.image2D_colorLookupTable[imgID]; int[] outputPixels = new
	 * int[pixCount];
	 *
	 * for (int i = 0; pixCount > i; ++i) { int color = colorLookupTable[255 &
	 * pixels[i]]; if (color == 0) { color = 1; } else if (color == 16711935) {
	 * color = 0; }
	 *
	 * outputPixels[i] = color; }
	 *
	 * this.surfaceSetPixels[imgID] = outputPixels; this.spriteColours[imgID] =
	 * null; this.image2D_colorLookupTable[imgID] = null; } } catch
	 * (RuntimeException var9) { throw GenUtil.makeThrowable(var9, "ua.P(" +
	 * imgID + ',' + -342059728 + ')'); } }
	 */

	private void plot_trans_scale_with_2_masks(int[] dest, int[] src, int destColumnCount,
											   int destColumnSkewPerRow, int destFirstColumn, int dummy1, int dummy2, int mask2, int scaleY, int scaleX,
											   int srcStartX, int skipEveryOther, int srcStartY, int srcWidth, int mask1, int destHeight,
											   int destRowHead) {
		plot_trans_scale_with_2_masks(dest, src, destColumnCount, destColumnSkewPerRow, destFirstColumn, dummy1, dummy2, mask2, scaleY, scaleX, srcStartX, skipEveryOther, srcStartY, srcWidth, mask1, destHeight, destRowHead, 0xFFFFFFFF, 0);
	}

	/**
	 * @param src                  source pixel data
	 * @param mask1                background color to show through when the source data is [x,
	 *                             x, x] (dest = background * source)
	 * @param mask2                background color to show through when the source data is [255,
	 *                             x, x] (dest = background * source)
	 * @param dummy1
	 * @param destColumnSkewPerRow increase in destination first column per destination row
	 * @param srcWidth             width of source data
	 * @param srcStartY            (source start row) << 16
	 * @param scaleY               (source rows per destination row) << 16
	 * @param destFirstColumn      first column to store to in the first destination row
	 * @param scaleX               (source columns per destination row) << 16
	 * @param dest                 destination pixel data
	 * @param skipEveryOther       if this is 0 or 1 the rasterizer skips every other destination
	 *                             pixel
	 * @param spritePixel
	 * @param srcStartX            (source start column) << 16
	 * @param destRowHead          pixel address of first column of the first row to store
	 * @param destColumnCount      destination column count
	 * @param destHeight           destination row count
	 * @param colourTransform      The colour and opacity with which to shade this sprite a uniform colour
	 */
	private void plot_trans_scale_with_2_masks(int[] dest, int[] src, int destColumnCount,
											   int destColumnSkewPerRow, int destFirstColumn, int dummy1, int spritePixel, int mask2, int scaleY, int scaleX,
											   int srcStartX, int skipEveryOther, int srcStartY, int srcWidth, int mask1, int destHeight,
											   int destRowHead, int colourTransform, int blueMask) {
		try {

			int mask1R = mask1 >> 16 & 0xFF;
			int mask1G = mask1 >> 8 & 0xFF;
			int mask1B = mask1 & 0xFF;
			int mask2R = mask2 >> 16 & 0xFF;
			int mask2G = mask2 >> 8 & 0xFF;
			int mask2B = mask2 & 0xFF;

			if (dummy1 != 1603920392) {
				this.clipBottom = 29;
			}

			if (blueMask == 0)
				blueMask = 0xFFFFFF;

			try {
				int var27 = srcStartX;

				for (int var28 = -destHeight; var28 < 0; ++var28) {
					int var29 = (srcStartY >> 16) * srcWidth;
					int var30 = destFirstColumn >> 16;
					int var31 = destColumnCount;
					int var32;
					if (this.clipLeft > var30) {
						var32 = this.clipLeft - var30;
						var31 = destColumnCount - var32;
						srcStartX += var32 * scaleX;
						var30 = this.clipLeft;
					}

					if (this.clipRight <= var30 + var31) {
						var32 = var30 - this.clipRight + var31;
						var31 -= var32;
					}

					skipEveryOther = 1 - skipEveryOther;
					if (skipEveryOther != 0) {
						for (var32 = var30; var30 + var31 > var32; ++var32) {
							spritePixel = src[var29 + (srcStartX >> 16)];
							if (spritePixel != 0) {
								int spritePixelR = spritePixel >> 16 & 0xFF;
								int spritePixelG = spritePixel >> 8 & 0xFF;
								int spritePixelB = spritePixel & 0xFF;

								// Is the colour from the sprite gray?
								if (spritePixelR == spritePixelG && spritePixelG == spritePixelB) {
									spritePixelR = (spritePixelR * mask1R) >> 8;
									spritePixelG = (spritePixelG * mask1G) >> 8;
									spritePixelB = (spritePixelB * mask1B) >> 8;
								} else if (spritePixelR == 255 && spritePixelG == spritePixelB) { // Is sprite colour full white?
									spritePixelR = (spritePixelR * mask2R) >> 8;
									spritePixelG = (spritePixelG * mask2G) >> 8;
									spritePixelB = (spritePixelB * mask2B) >> 8;
								} else if (blueMask != 0xFFFFFF && spritePixelR == spritePixelG && spritePixelB != spritePixelG) {
									int blueMaskR = blueMask >> 16 & 0xFF;
									int blueMaskG = blueMask >> 8 & 0xFF;
									int blueMaskB = blueMask & 0xFF;
									int shifter = spritePixelR*spritePixelB;
									spritePixelR = (blueMaskR * shifter) >> 16;
									spritePixelG = (blueMaskG * shifter) >> 16;
									spritePixelB = (blueMaskB * shifter) >> 16;
								}

								int opacity = colourTransform >> 24 & 0xFF;
								int inverseOpacity = 0xFF - opacity;

								int transformR = (colourTransform >> 16) & 0xFF;
								int transformG = (colourTransform >> 8) & 0xFF;
								int transformB = colourTransform & 0xFF;

								int spriteR = ((spritePixelR * transformR) >> 8) * opacity;
								int spriteG = ((spritePixelG * transformG) >> 8) * opacity;
								int spriteB = ((spritePixelB * transformB) >> 8) * opacity;

								int canvasR = (dest[var32 + destRowHead] >> 16 & 0xff) * inverseOpacity;
								int canvasG = (dest[var32 + destRowHead] >> 8 & 0xff) * inverseOpacity;
								int canvasB = (dest[var32 + destRowHead] & 0xff) * inverseOpacity;

								int finalColour =
										(((spriteR + canvasR) >> 8) << 16) +
												(((spriteG + canvasG) >> 8) << 8) +
												((spriteB + canvasB) >> 8);
								dest[var32 + destRowHead] = finalColour;

								/*if (spritePixelR == spritePixelG && spritePixelB == spritePixelG) {
									dest[var32 + destRowHead] = (spritePixelB * mask1B >> 8) + (mask1G * spritePixelG >> 8 << 8)
											+ (spritePixelR * mask1R >> 8 << 16);
								} else if (spritePixelR == 255 && spritePixelG == spritePixelB) {
									dest[var32 + destRowHead] = (mask2B * spritePixelB >> 8) + (spritePixelR * mask2R >> 8 << 16)
											+ (spritePixelG * mask2G >> 8 << 8);
								} else {
									dest[var32 + destRowHead] = spritePixel;
								}*/
							}

							srcStartX += scaleX;
						}
					}

					srcStartY += scaleY;
					srcStartX = var27;
					destRowHead += this.width2;
					destFirstColumn += destColumnSkewPerRow;
				}
			} catch (Exception var33) {
				System.out.println("error in transparent sprite plot routine");
			}

		} catch (RuntimeException var34) {
			throw GenUtil.makeThrowable(var34,
					"ua.AA(" + (dest != null ? "{...}" : "null") + ',' + (src != null ? "{...}" : "null") + ','
							+ destColumnCount + ',' + destColumnSkewPerRow + ',' + destFirstColumn + ',' + dummy1 + ','
							+ spritePixel + ',' + mask2 + ',' + scaleY + ',' + scaleX + ',' + srcStartX + ','
							+ skipEveryOther + ',' + srcStartY + ',' + srcWidth + ',' + mask1 + ',' + destHeight + ','
							+ destRowHead + ')');
		}
	}

	public final void copyPixelDataToSurface(SPRITE_LAYER layer, int xOffset, int yOffset, int width, int height) {
		try {
			int[] pixels = new int[width * height];
			int pixel = 0;
			for (int x = xOffset; x < xOffset + width; x++) {
				for (int y = yOffset; y < yOffset + height; y++) {
					pixels[pixel++] = pixelData[x + y * width2];
				}
			}

			Sprite sprite = new Sprite(pixels, width, height);
			sprite.setShift(0, 0);
			sprite.setRequiresShift(false);
			sprite.setSomething(width, height);

			switch (layer) {
				case MINIMAP:
					minimapSprite = sprite;
					break;
				case WORLDMAP:
					//doesn't look like the worldmap is generated on hte fly
					//sprites[4500] = sprite;
					break;
				case SHOP:
					//sprites[49] = sprite;
					break;
			}

			/*
			 *  this.image2D_width[destLayer] = width;
			 * this.image2D_height[destLayer] = height;
			 * this.image2D_hasAlpha[destLayer] = false;
			 * this.image2D_xOffset[destLayer] = 0;
			 * this.image2D_yOffset[destLayer] = 0;
			 * this.image2D_setParam2[destLayer] = width;
			 * this.image2D_setParam3[destLayer] = height; int count = height *
			 * width; int destHead = 0; this.surfaceSetPixels[destLayer] = new
			 * int[count];
			 *
			 * for (int x = xOffset; xOffset + width > x; ++x) { for (int y =
			 * yOffset; y < height + yOffset; ++y) {
			 * this.surfaceSetPixels[destLayer][destHead++] = this.pixelData[x +
			 * this.width2 * y]; } }
			 */
		} catch (RuntimeException var11) {
			throw GenUtil.makeThrowable(var11, "ua.BB(" + height + ',' + xOffset + ',' + yOffset + ',' + "dummy" + ','
					+ layer + ',' + width + ')');
		}
	}

	public final void drawVerticalGradient(int x, int y, int width, int height, int topColor, int bottomColor) {
		try {

			if (this.clipLeft > x) {
				width -= this.clipLeft - x;
				x = this.clipLeft;
			}

			if (this.clipRight < width + x) {
				width = this.clipRight - x;
			}

			int topR = (topColor & 0xFF0000) >> 16;
			int topG = topColor >> 8 & 255;
			int topB = topColor & 255;
			int btmR = (bottomColor & 0xFF0000) >> 16;
			int btmG = bottomColor >> 8 & 255;
			int btmB = bottomColor & 255;
			int rowStride = this.width2 - width;
			byte yStep = 1;
			if (this.interlace) {
				rowStride += this.width2;
				yStep = 2;
				if ((y & 1) != 0) {
					++y;
					--height;
				}
			}
			int pxHead = x + this.width2 * y;

			for (int yi = 0; height > yi; yi += yStep) {
				if (this.clipTop <= yi + y && y + yi < this.clipBottom) {
					int color = ((topG * yi + btmG * (height - yi)) / height << 8)
							+ ((btmR * (height - yi) + topR * yi) / height << 16)
							+ (yi * topB + btmB * (height - yi)) / height;

					for (int xi = -width; xi < 0; ++xi) {
						this.pixelData[pxHead++] = color;
					}

					pxHead += rowStride;
				} else {
					pxHead += this.width2;
				}
			}

		} catch (RuntimeException var20) {
			throw GenUtil.makeThrowable(var20, "ua.F(" + x + ',' + bottomColor + ',' + width + ',' + topColor + ','
					+ height + ',' + y + ',' + "dummy" + ')');
		}
	}

	public final void drawBoxBorder(int x, int width, int y, int height, int color) {
		try {
			this.drawLineHoriz(x, y, width, color);

			this.drawLineHoriz(x, height - 1 + y, width, color);
			this.drawLineVert(x, y, color, height);
			this.drawLineVert(width + x - 1, y, color, height);
		} catch (RuntimeException var8) {
			throw GenUtil.makeThrowable(var8,
					"ua.U(" + x + ',' + width + ',' + y + ',' + "dummy" + ',' + height + ',' + color + ')');
		}
	}

	public final void drawLineHoriz(int x, int y, int width, int color) {
		try {

			if (this.clipTop <= y && y < this.clipBottom) {
				if (this.clipLeft > x) {
					width -= this.clipLeft - x;
					x = this.clipLeft;
				}

				if (x + width > this.clipRight) {
					width = this.clipRight - x;
				}

				if (width > 0) {
					int offset = x + this.width2 * y;

					for (int xi = 0; width > xi; ++xi) {
						this.pixelData[offset + xi] = color;
					}

				}
			}
		} catch (RuntimeException var9) {
			throw GenUtil.makeThrowable(var9, "ua.LB(" + width + ',' + color + ',' + x + ',' + y + ',' + "dummy" + ')');
		}
	}

	public final void drawLineVert(int x, int y, int color, int height) {

		if (this.clipLeft <= x && x < this.clipRight) {
			if (y < this.clipTop) {
				height -= this.clipTop - y;
				y = this.clipTop;
			}

			if (y + height > this.clipBottom) {
				height = this.clipBottom - y;
			}

			if (height > 0) {
				int pxOffset = x + this.width2 * y;

				for (int i = 0; height > i; ++i) {
					this.pixelData[pxOffset + this.width2 * i] = color;
				}
			}
		}

	}

	public final void drawSprite(Sprite sprite, int x, int y) {
		try {
			if (sprite == null) {
				System.out.println("sprite missing:" + sprite);
				return;
			}
			if (sprite.requiresShift()) {
				x += sprite.getXShift();
				y += sprite.getYShift();
			}

			int var5 = y * this.width2 + x;
			int var6 = 0;
			int var7 = sprite.getHeight();
			int var8 = sprite.getWidth();
			int var9 = this.width2 - var8;
			int var10 = 0;
			int var11;
			if (this.clipTop > y) {
				var11 = this.clipTop - y;
				var7 -= var11;
				y = this.clipTop;
				var5 += this.width2 * var11;
				var6 += var11 * var8;
			}

			if (y + var7 >= this.clipBottom) {
				var7 -= 1 + (var7 + y - this.clipBottom);
			}

			if (x < this.clipLeft) {
				var11 = this.clipLeft - x;
				var6 += var11;
				var9 += var11;
				var8 -= var11;
				var10 += var11;
				x = this.clipLeft;
				var5 += var11;
			}

			if (x + var8 >= this.clipRight) {
				var11 = x + var8 - this.clipRight + 1;
				var8 -= var11;
				var10 += var11;
				var9 += var11;
			}

			if (var8 > 0 && var7 > 0) {
				byte var13 = 1;
				if (this.interlace) {
					var9 += this.width2;
					if ((1 & y) != 0) {
						var5 += this.width2;
						--var7;
					}

					var13 = 2;
					var10 += sprite.getWidth();
				}

				this.a(var8, this.pixelData, var13, var7, 0, var6, (byte) 123, var5, sprite.getPixels(), var9,
						var10);

			}
		} catch (RuntimeException var12) {
			throw GenUtil.makeThrowable(var12, "ua.Q(" + -1 + ',' + sprite + ',' + y + ',' + x + ')');
		}
	}

	public final void drawSprite(Sprite sprite, int x, int y, int destWidth, int destHeight, int var5) {
		try {
			try {
				int spriteWidth = sprite.getWidth();
				int spriteHeight = sprite.getHeight();
				int srcStartX = 0;
				int srcStartY = 0;
				int scaleX = (spriteWidth << 16) / destWidth;
				int scaleY = (spriteHeight << 16) / destHeight;
				int destHead;
				int destRowStride;
				if (sprite.requiresShift()) {
					destHead = sprite.getSomething1();
					destRowStride = sprite.getSomething2();
					if (destHead == 0 || destRowStride == 0) {
						return;
					}

					if (sprite.getYShift() * destHeight % destRowStride != 0) {
						srcStartY = (destRowStride - destHeight * sprite.getYShift() % destRowStride << 16)
								/ destHeight;
					}

					scaleX = (destHead << 16) / destWidth;
					if (sprite.getXShift() * destWidth % destHead != 0) {
						srcStartX = (destHead - sprite.getXShift() * destWidth % destHead << 16) / destWidth;
					}

					x += (destWidth * sprite.getXShift() + destHead - 1) / destHead;
					scaleY = (destRowStride << 16) / destHeight;
					y += (destRowStride + destHeight * sprite.getYShift() - 1) / destRowStride;
					destHeight = (sprite.getHeight() - (srcStartY >> 16)) * destHeight / destRowStride;
					destWidth = destWidth * (sprite.getWidth() - (srcStartX >> 16)) / destHead;
				}

				destHead = x + this.width2 * y;
				if (y < this.clipTop) {
					int lost = this.clipTop - y;
					srcStartY += scaleY * lost;
					destHeight -= lost;
					destHead += this.width2 * lost;
					y = 0;
				}

				destRowStride = this.width2 - destWidth;
				if (y + destHeight >= this.clipBottom) {
					destHeight -= y - this.clipBottom + destHeight + 1;
				}

				if (x < this.clipLeft) {
					int lost = this.clipLeft - x;
					destWidth -= lost;
					destRowStride += lost;
					destHead += lost;
					x = 0;
					srcStartX += scaleX * lost;
				}

				if (this.clipRight <= x + destWidth) {
					int lost = 1 + x + (destWidth - this.clipRight);
					destRowStride += lost;
					destWidth -= lost;
				}

				byte heightStep = 1;
				if (this.interlace) {
					if ((y & 1) != 0) {
						--destHeight;
						destHead += this.width2;
					}

					destRowStride += this.width2;
					heightStep = 2;
					scaleY += scaleY;
				}

				this.plot_scale_black_mask(sprite.getPixels(), heightStep, scaleX, 0, srcStartY,
						this.pixelData, (byte) 78, scaleY, destHeight, srcStartX, destRowStride, destWidth, spriteWidth,
						destHead);
			} catch (Exception var16) {
				System.out.println("error in sprite clipping routine");
			}

		} catch (RuntimeException var17) {
			throw GenUtil.makeThrowable(var17,
					"ua.D(" + x + ',' + y + ',' + destHeight + ',' + destWidth + ',' + 5924 + ',' + sprite + ')');
		}
	}

	public final void setPixel(int x, int y, int val) {
		try {

			if (this.clipLeft <= x && this.clipTop <= y && this.clipRight > x && this.clipBottom > y) {
				this.pixelData[x + this.width2 * y] = val;
			}
		} catch (RuntimeException var6) {
			throw GenUtil.makeThrowable(var6, "ua.CB(" + y + ',' + x + ',' + "dummy" + ',' + val + ')');
		}
	}

	public final void drawSpriteClipping(Sprite sprite, int x, int y, int width, int height, int colorMask, int colorMask2, int blueMask,
										 boolean mirrorX, int topPixelSkew, int dummy) {
		drawSpriteClipping(sprite, x, y, width, height, colorMask, colorMask2, blueMask, mirrorX, topPixelSkew, dummy, 0xFFFFFFFF);
	}

	public final void drawSpriteClipping(Sprite e, int x, int y, int width, int height, int colorMask, int colorMask2, int blueMask,
										 boolean mirrorX, int topPixelSkew, int dummy, int colourTransform) {
		try {
			try {

				if (colorMask2 == 0) {
					colorMask2 = 0xFFFFFF;
				}

				if (colorMask == 0) {
					colorMask = 0xFFFFFF;
				}

				int spriteWidth = e.getWidth();
				int spriteHeight = e.getHeight();
				int srcStartX = 0;
				int srcStartY = 0;
				int destFirstColumn = topPixelSkew << 16;
				int scaleX = (spriteWidth << 16) / width;
				int scaleY = (spriteHeight << 16) / height;
				int destColumnSkewPerRow = -(topPixelSkew << 16) / height;
				int destRowHead;
				int skipEveryOther;
				if (e.requiresShift()) {
					destRowHead = e.getSomething1();
					skipEveryOther = e.getSomething2();
					if (destRowHead == 0 || skipEveryOther == 0) {
						return;
					}

					scaleX = (destRowHead << 16) / width;
					scaleY = (skipEveryOther << 16) / height;
					int var21 = e.getXShift();
					if (mirrorX) {
						var21 = destRowHead - e.getWidth() - var21;
					}

					int var22 = e.getYShift();
					x += (destRowHead + var21 * width - 1) / destRowHead;
					int var23 = (var22 * height + skipEveryOther - 1) / skipEveryOther;
					if (var21 * width % destRowHead != 0) {
						srcStartX = (destRowHead - width * var21 % destRowHead << 16) / width;
					}

					y += var23;
					destFirstColumn += var23 * destColumnSkewPerRow;
					if (var22 * height % skipEveryOther != 0) {
						srcStartY = (skipEveryOther - height * var22 % skipEveryOther << 16) / height;
					}

					width = (scaleX + ((e.getWidth() << 16) - (srcStartX + 1))) / scaleX;
					height = ((e.getHeight() << 16) - srcStartY - (1 - scaleY)) / scaleY;
				}

				destRowHead = this.width2 * y;
				destFirstColumn += x << 16;
				if (y < this.clipTop) {
					skipEveryOther = this.clipTop - y;
					destFirstColumn += destColumnSkewPerRow * skipEveryOther;
					height -= skipEveryOther;
					srcStartY += skipEveryOther * scaleY;
					destRowHead += this.width2 * skipEveryOther;
					y = this.clipTop;
				}

				if (y + height >= this.clipBottom) {
					height -= 1 + y + height - this.clipBottom;
				}

				skipEveryOther = destRowHead / this.width2 & dummy;
				if (!this.interlace) {
					skipEveryOther = 2;
				}
				// TODO:Make sure this works.
				if (colorMask2 == 0xFFFFFF) {
					if (null != e.getPixels()) {
						if (mirrorX) {
							this.plot_tran_scale_with_mask(dummy ^ 74, e.getPixels(), scaleY, 0,
									srcStartY, (e.getWidth() << 16) - (srcStartX + 1), width,
									this.pixelData, height, destColumnSkewPerRow, destRowHead, -scaleX, destFirstColumn,
									spriteWidth, skipEveryOther, colorMask, colourTransform, blueMask);
						} else {
							this.plot_tran_scale_with_mask(dummy + 89, e.getPixels(), scaleY, 0,
									srcStartY, srcStartX, width, this.pixelData, height, destColumnSkewPerRow,
									destRowHead, scaleX, destFirstColumn, spriteWidth, skipEveryOther, colorMask, colourTransform, blueMask);
						}
					}
				} else if (mirrorX) {
					this.plot_trans_scale_with_2_masks(this.pixelData, e.getPixels(), width,
							destColumnSkewPerRow, destFirstColumn, dummy + 1603920391, 0, colorMask2, scaleY, -scaleX,
							(e.getWidth() << 16) - srcStartX - 1, skipEveryOther, srcStartY, spriteWidth,
							colorMask, height, destRowHead, colourTransform, blueMask);
				} else {
					this.plot_trans_scale_with_2_masks(this.pixelData, e.getPixels(), width,
							destColumnSkewPerRow, destFirstColumn, 1603920392, 0, colorMask2, scaleY, scaleX, srcStartX,
							skipEveryOther, srcStartY, spriteWidth, colorMask, height, destRowHead, colourTransform, blueMask);
				}
			} catch (Exception var24) {
				System.out.println("error in sprite clipping routine");
			}

		} catch (RuntimeException var25) {
			throw GenUtil.makeThrowable(var25, "ua.AB(" + y + ',' + colorMask + ',' + colorMask2 + ',' + mirrorX + ','
					+ topPixelSkew + ',' + e + ',' + height + ',' + width + ',' + x + ',' + dummy + ')');
		}
	}

	/**
	 * destHeight is height / heightStep
	 *
	 * @param heightStep    destinationHeight / height
	 * @param srcStartY     First source row << 16
	 * @param destWidth     Destination width
	 * @param dummy1
	 * @param scaleY        (source rows per destination row) << 16
	 * @param spriteWidth   columns in source data
	 * @param scaleX        (source columns per destination column) << 16
	 * @param height        destination height * heightStep
	 * @param destHead      first destination pixel to output to
	 * @param src           source pixel data
	 * @param dummy2
	 * @param srcStartX     First source column << 16
	 * @param destRowStride Pixels to skip between rows of output
	 * @param alpha         Alpha value [0-256]
	 * @param dest          Destination pixel data
	 */
	private void plot_tran_scale(int heightStep, int srcStartY, int destWidth, byte dummy1, int scaleY,
								 int spriteWidth, int scaleX, int height, int destHead, int[] src, int dummy2, int srcStartX,
								 int destRowStride, int alpha, int[] dest) {
		try {

			int alphaInverse = 256 - alpha;

			try {
				int firstColumn = srcStartX;
				// destHeight = height / heightStep
				for (int i = -height; i < 0; i += heightStep) {
					int rowOffset = spriteWidth * (srcStartY >> 16);
					srcStartY += scaleY;

					for (int j = -destWidth; j < 0; ++j) {
						int newColor = src[rowOffset + (srcStartX >> 16)];
						srcStartX += scaleX;
						if (newColor == 0) {
							++destHead;
						} else {
							int oldColor = dest[destHead];
							dest[destHead++] = FastMath
									.bitwiseAnd(FastMath.bitwiseAnd(0xFF00, oldColor) * alphaInverse
											+ FastMath.bitwiseAnd(0xFF00, newColor) * alpha, 0xFF0000)
									+ FastMath.bitwiseAnd(
									FastMath.bitwiseAnd(newColor, 0xFF00FF) * alpha
											+ alphaInverse * FastMath.bitwiseAnd(0xFF00FF, oldColor),
									-16711936) >> 8;
						}
					}

					destHead += destRowStride;
					srcStartX = firstColumn;
				}
			} catch (Exception var22) {
				System.out.println("error in tran_scale");
			}

		} catch (RuntimeException var23) {
			throw GenUtil.makeThrowable(var23,
					"ua.EA(" + heightStep + ',' + srcStartY + ',' + destWidth + ',' + dummy1 + ',' + scaleY + ','
							+ spriteWidth + ',' + scaleX + ',' + height + ',' + destHead + ','
							+ (src != null ? "{...}" : "null") + ',' + dummy2 + ',' + srcStartX + ',' + destRowStride
							+ ',' + alpha + ',' + (dest != null ? "{...}" : "null") + ')');
		}
	}

	private void plot_tran_scale_with_mask(int dummy2, int[] src, int scaleY, int dummy1, int srcStartY,
										   int srcStartX, int destColumnCount, int[] dest, int destHeight, int destColumnSkewPerRow, int destRowHead,
										   int scaleX, int destFirstColumn, int srcWidth, int skipEveryOther, int background) {
		plot_tran_scale_with_mask(dummy2, src, scaleY, dummy1, srcStartY, srcStartX, destColumnCount, dest, destHeight, destColumnSkewPerRow, destRowHead, scaleX, destFirstColumn, srcWidth, skipEveryOther, background, 0xFFFFFFFF);
	}

	/**
	 * @param dummy2
	 * @param src                  source pixel data
	 * @param scaleY               (source rows per destination row) << 16
	 * @param dummy1
	 * @param srcStartY            (source start row) << 16
	 * @param srcStartX            (source start column) << 16
	 * @param destColumnCount      destination column count
	 * @param dest                 destination pixel data
	 * @param destHeight           destination row count
	 * @param destColumnSkewPerRow increase in destination first column per destination row
	 * @param destRowHead          pixel address of first column of the first row to store
	 * @param scaleX               (source columns per destination row) << 16
	 * @param destFirstColumn      first column to store to in the first destination row
	 * @param srcWidth             width of source data
	 * @param skipEveryOther       if this is 0 or 1 the rasterizer skips every other destination
	 *                             pixel
	 * @param spritePixel          background color to show through when the source data is grey
	 *                             (dest = background * source)
	 * @param colourTransform      The colour and opacity with which to shade this sprite a uniform colour
	 */
	private void plot_tran_scale_with_mask(int dummy2, int[] src, int scaleY, int dummy1, int srcStartY,
										   int srcStartX, int destColumnCount, int[] dest, int destHeight, int destColumnSkewPerRow, int destRowHead,
										   int scaleX, int destFirstColumn, int srcWidth, int skipEveryOther, int spritePixel, int colourTransform) {
		this.plot_tran_scale_with_mask(dummy2, src, scaleY, dummy1, srcStartY, srcStartX, destColumnCount, dest, destHeight, destColumnSkewPerRow, destRowHead,
				scaleX, destFirstColumn, srcWidth, skipEveryOther, spritePixel, colourTransform, 0);
	}
	private void plot_tran_scale_with_mask(int dummy2, int[] src, int scaleY, int dummy1, int srcStartY,
										   int srcStartX, int destColumnCount, int[] dest, int destHeight, int destColumnSkewPerRow, int destRowHead,
										   int scaleX, int destFirstColumn, int srcWidth, int skipEveryOther, int spritePixel, int colourTransform, int blueMask) {
		try {

			int spritePixelR = spritePixel >> 16 & 0xFF;
			int spritePixelG = spritePixel >> 8 & 0xFF;
			int spritePixelB = spritePixel & 0xFF;

			if (blueMask == 0)
				blueMask = 0xFFFFFF;
			try {
				int firstColumn = srcStartX;

				for (int i = -destHeight; i < 0; ++i) {
					int srcRowHead = (srcStartY >> 16) * srcWidth;
					int duFirstColumn = destFirstColumn >> 16;
					int duColumnCount = destColumnCount;
					if (duFirstColumn < this.clipLeft) {
						int lost = this.clipLeft - duFirstColumn;
						duFirstColumn = this.clipLeft;
						duColumnCount = destColumnCount - lost;
						srcStartX += scaleX * lost;
					}

					skipEveryOther = 1 - skipEveryOther;
					if (duFirstColumn + duColumnCount >= this.clipRight) {
						int lost = duColumnCount + duFirstColumn - this.clipRight;
						duColumnCount -= lost;
					}

					if (skipEveryOther != 0) {
						for (int j = duFirstColumn; j < duColumnCount + duFirstColumn; ++j) {
							int newColor = src[srcRowHead + (srcStartX >> 16)];
							if (newColor != 0) {
								int opacity = colourTransform >> 24 & 0xFF;
								int inverseOpacity = 256 - opacity;

								int transformR = colourTransform >> 16 & 0xFF;
								int transformG = colourTransform >> 8 & 0xFF;
								int transformB = colourTransform & 0xFF;

								int newR = newColor >> 16 & 0xFF;
								int newG = newColor >> 8 & 0xFF;
								int newB = newColor & 0xFF;

								// Is the colour from the sprite gray?
								if (newR == newG && newG == newB) {
									newR = (spritePixelR * newR) >> 8;
									newG = (spritePixelG * newG) >> 8;
									newB = (spritePixelB * newB) >> 8;
								} else if (blueMask != 0xFFFFFF && newR == newG && newB != newR) {//blue mask?
									int blueMaskR = blueMask >> 16 & 0xFF;
									int blueMaskG = blueMask >> 8 & 0xFF;
									int blueMaskB = blueMask & 0xFF;
									int shifter = newR*newB;
									newR = (blueMaskR * shifter) >> 16;
									newG = (blueMaskG * shifter) >> 16;
									newB = (blueMaskB * shifter) >> 16;
								}

								int spriteR = ((newR * transformR) >> 8) * opacity;
								int spriteG = ((newG * transformG) >> 8) * opacity;
								int spriteB = ((newB * transformB) >> 8) * opacity;

								int canvasR = (dest[destRowHead + j] >> 16 & 0xff) * inverseOpacity;
								int canvasG = (dest[destRowHead + j] >> 8 & 0xff) * inverseOpacity;
								int canvasB = (dest[destRowHead + j] & 0xff) * inverseOpacity;

								int finalColour =
										(((spriteR + canvasR) >> 8) << 16) +
												(((spriteG + canvasG) >> 8) << 8) +
												((spriteB + canvasB) >> 8);
								dest[destRowHead + j] = finalColour;

								/*// Are we a grey?
								if (newR == newG && newB == newG) {
									dest[destRowHead + j] = (newB * backgroundB >> 8) + ((backgroundG * newG >> 8) << 8)
											+ ((backgroundR * newR >> 8) << 16);
								} else {
									dest[destRowHead + j] = newColor;
								}*/
							}

							srcStartX += scaleX;
						}
					}

					srcStartY += scaleY;
					srcStartX = firstColumn;
					destFirstColumn += destColumnSkewPerRow;
					destRowHead += this.width2;
				}
			} catch (Exception var29) {
				System.out.println("error in transparent sprite plot routine");
			}

			if (dummy2 < 20) {
				this.m_t = (int[]) null;
			}

		} catch (RuntimeException var30) {
			throw GenUtil.makeThrowable(var30,
					"ua.GA(" + dummy2 + ',' + (src != null ? "{...}" : "null") + ',' + scaleY + ',' + dummy1 + ','
							+ srcStartY + ',' + srcStartX + ',' + destColumnCount + ','
							+ (dest != null ? "{...}" : "null") + ',' + destHeight + ',' + destColumnSkewPerRow + ','
							+ destRowHead + ',' + scaleX + ',' + destFirstColumn + ',' + srcWidth + ',' + skipEveryOther
							+ ',' + spritePixel + ')');
		}
	}

	/**
	 * @param src                  source pixel data
	 * @param background           background color to show through when the source data is grey
	 *                             (dest = background * source)
	 * @param dummy1
	 * @param destColumnSkewPerRow increase in destination first column per destination row
	 * @param lookupTable          mapping from source value to color
	 * @param srcWidth             width of source data
	 * @param srcStartY            (source start row) << 16
	 * @param scaleY               (source rows per destination row) << 16
	 * @param destFirstColumn      first column to store to in the first destination row
	 * @param scaleX               (source columns per destination row) << 16
	 * @param dest                 destination pixel data
	 * @param skipEveryOther       if this is 0 or 1 the rasterizer skips every other destination
	 *                             pixel
	 * @param dummy2
	 * @param srcStartX            (source start column) << 16
	 * @param destRowHead          pixel address of first column of the first row to store
	 * @param destColumnCount      destination column count
	 * @param destHeight           destination row count
	 */
	final void plot_trans_scale_lookup_with_mask(byte[] src, int background, int dummy1, int destColumnSkewPerRow,
												 int[] lookupTable, int srcWidth, int srcStartY, int scaleY, int destFirstColumn, int scaleX, int[] dest,
												 int skipEveryOther, int dummy2, int srcStartX, int destRowHead, int destColumnCount, int destHeight) {
		try {

			int backR = (0xFF0000 & background) >> 16;
			int backG = (0xFF00 & background) >> 8;
			int backB = background & 255;

			try {
				int firstColumn = srcStartX;

				for (int i = -destHeight; i < 0; ++i) {
					int srcRowHead = (srcStartY >> 16) * srcWidth;
					int duStartCol = destFirstColumn >> 16;
					int duColCount = destColumnCount;
					if (duStartCol < this.clipLeft) {
						int tmp = this.clipLeft - duStartCol;
						duStartCol = this.clipLeft;
						duColCount = destColumnCount - tmp;
						srcStartX += tmp * scaleX;
					}

					if (duColCount + duStartCol >= this.clipRight) {
						int lost = duStartCol + (duColCount - this.clipRight);
						duColCount -= lost;
					}

					skipEveryOther = 1 - skipEveryOther;
					srcStartY += scaleY;
					if (skipEveryOther != 0) {
						for (int j = duStartCol; j < duColCount + duStartCol; ++j) {
							int newColor = src[(srcStartX >> 16) + srcRowHead] & 255;
							if (newColor != 0) {
								newColor = lookupTable[newColor];
								int newB = newColor & 255;
								int newR = newColor >> 16 & 255;
								int newG = newColor >> 8 & 255;
								// Is grey
								if (newR == newG && newB == newG) {
									dest[j + destRowHead] = (backG * newG >> 8 << 8) + (backR * newR >> 8 << 16)
											+ (backB * newB >> 8);
								} else {
									dest[destRowHead + j] = newColor;
								}
							}

							srcStartX += scaleX;
						}
					}

					destFirstColumn += destColumnSkewPerRow;
					destRowHead += this.width2;
					srcStartX = firstColumn;
				}
			} catch (Exception var31) {
				System.out.println("error in transparent sprite plot routine");
			}

		} catch (RuntimeException var32) {
			throw GenUtil.makeThrowable(var32,
					"ua.IB(" + (src != null ? "{...}" : "null") + ',' + background + ',' + dummy1 + ','
							+ destColumnSkewPerRow + ',' + (lookupTable != null ? "{...}" : "null") + ',' + srcWidth
							+ ',' + srcStartY + ',' + scaleY + ',' + destFirstColumn + ',' + scaleX + ','
							+ (dest != null ? "{...}" : "null") + ',' + skipEveryOther + ',' + dummy2 + ',' + srcStartX
							+ ',' + destRowHead + ',' + destColumnCount + ',' + destHeight + ')');
		}
	}

//	public boolean fillSpriteTree() {
//		File workspaceFile = new File(Config.F_CACHE_DIR, "video" + File.separator + "Custom_Sprites.osar");
//		if (!workspaceFile.exists())
//			return false;
//
//		Unpacker unpacker = new Unpacker();
//		Workspace workspace = unpacker.unpackArchive(workspaceFile);
//		for (Subspace subspace : workspace.getSubspaces()) {
//			Map<String, Entry> entries = new HashMap<>();
//			for (Entry entry : subspace.getEntryList())
//				entries.put(entry.getID(), entry);
//			spriteTree.put(subspace.getName(), entries);
//		}
//
//		return true;
//	}

	private static String readString(ByteBuffer buffer) {
		StringBuilder bldr = new StringBuilder();

		byte b;
		try {
			while ((b = buffer.get()) != 10) {
				bldr.append((char) b);
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return bldr.toString();
	}
}
