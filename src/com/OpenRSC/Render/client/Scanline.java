package com.OpenRSC.Render.client;

final public class Scanline {
	int m_e;
	int upperBoundary;
	int lowerBoundary;
	int m_l;

	public int getM_e() { return this.m_e; }
	public int getLowerBoundary() {
		return lowerBoundary;
	}
	public int getM_l() {
		return m_l;
	}
	public int getUpperBoundary() {
		return upperBoundary;
	}

	public void setM_e(int a) { m_e = a; }

	public void setLowerBoundary(int lowerBoundary) {
		this.lowerBoundary = lowerBoundary;
	}

	public void setUpperBoundary(int upperBoundary) {
		this.upperBoundary = upperBoundary;
	}

	public void setM_l(int m_l) {
		this.m_l = m_l;
	}
}
