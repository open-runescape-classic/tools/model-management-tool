package com.OpenRSC.Render;

public class SphericalCoordinates {

    double phi;
    double theta;
    double rho;

    public SphericalCoordinates(double phi, double theta, double rho) {
        this.phi = phi;
        this.rho = rho;
        this.theta = theta;
    }

    public double incPhi(double inc) {
        this.phi += inc;
        return this.phi;
    }

    public double incTheta(double inc) {
        this.theta += inc;
        return this.theta;
    }
    public double incRho(double inc) {
        this.rho += inc;
        return this.rho;
    }

    public double getPhi() { return this.phi; }
    public double getTheta() { return this.theta; }
    public double getRho() { return this.rho; }

    public double getXTranslate() {
        return -(int)(rho * Math.sin(phi) * Math.sin(theta));
    }
    public double getYTranslate() {
        return (int)(rho * Math.cos(phi));
    }
    public double getZTranslate() {
        return (int)(rho * Math.sin(phi) * Math.cos(theta));
    }
    public double getXRotate() {
        return 1024*(90-Math.toDegrees(phi))/360;
    }
    public double getYRotate() {
        return 1536 - 1024 *(Math.toDegrees(theta)/360);
    }
    public double getZRotate() {
        return 0;
    }
}
