package com.OpenRSC.Render;

import com.OpenRSC.IO.texture.Entry;
import com.OpenRSC.IO.texture.Frame;
import com.OpenRSC.IO.texture.TextureUnpacker;
import com.OpenRSC.ModelTool;
import com.OpenRSC.Render.client.GraphicsController;
import com.OpenRSC.Render.client.RSModel;
import com.OpenRSC.Render.client.Scanline;
import com.OpenRSC.Render.client.Scene;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;


public class ModelRenderer implements Runnable {

    private final ModelTool modelTool;
    private final ScheduledExecutorService scheduledExecutor;

    private volatile Boolean running = false;

    private int width;
    private int height;

    private GraphicsController graphicsController;
    private com.OpenRSC.Render.client.Scene sceneRender;
    private ImageView viewer;

    private int anchorX;
    private int anchorY;

    private int mouseX;
    private int mouseY;

    private boolean controlDown = false;

    private int cameraPitch = 0;
    private int cameraRotation = 0;
    private int cameraYaw = 0;
    private int cameraCenterX = 0;
    private int cameraCenterY = 0;
    private int cameraCenterZ = 0;
    private int cameraZoom = 400;

    private double mouseControlSpeed = 1.0;

    private RSModel currentModel;
    private SimpleIntegerProperty selectedFace = new SimpleIntegerProperty();
    private ObservableList<Integer> usedFillers = FXCollections.observableArrayList();

    public ModelRenderer(ModelTool modelTool, ImageView viewer) {
        this.selectedFace.set(-1);
        this.modelTool = modelTool;
        this.scheduledExecutor = Executors.newSingleThreadScheduledExecutor();
        this.viewer = viewer;
        this.width = (int) viewer.getFitWidth();
        this.height = (int) viewer.getFitHeight();
        graphicsController = new GraphicsController(width, height);
        sceneRender = new com.OpenRSC.Render.client.Scene(graphicsController, 15000, 15000, 1000);
        sceneRender.setMidpoints(height / 2, true, width, width / 2, height / 2, 9, width / 2);
        sceneRender.fogLandscapeDistance = 2400;
        sceneRender.fogEntityDistance = 2400;
        sceneRender.fogSmoothingStartDistance = 2300;
        sceneRender.fogZFalloff = 1;
        sceneRender.setDiffuseDir(-50, -10, true, -50);

        modelTool.getMainController().linkFillList(usedFillers);

        loadTextures();
        start();
    }

    public void start() {
        synchronized (running) {
            this.running = true;
            // Schedule for 50FPS ... This is not the authentic way the client limits the framerate
            scheduledExecutor.scheduleAtFixedRate(this, 0, 20, TimeUnit.MILLISECONDS);
        }
    }

    public void stop() {
        synchronized (running) {
            this.running = false;
            scheduledExecutor.shutdown();
            System.out.println("stop() called");
        }
    }

    private void loadTextures() {
        File textureFolder = new File("res/textures");
        if (!textureFolder.exists())
            return;

        TextureUnpacker unpacker = new TextureUnpacker();

        File[] entryFiles = textureFolder.listFiles(File::isFile);
        sceneRender.setFrustum(0, 11, 7, entryFiles.length);
        try {
            for (int i = 0; i < entryFiles.length; ++i) {
                File entryFile = new File(textureFolder, i + ".ospr");
                if (!entryFile.exists())
                    continue;
                if (FilenameUtils.getExtension(entryFile.getName()).equalsIgnoreCase("ospr")) {
                    Entry entry = unpacker.unpackEntry(entryFile);
                    Frame texture = entry.getFrames()[0];
                    modelTool.addAvailableTexture(entry);
                    int length = texture.getWidth() * texture.getHeight();
                    int[] pixels = texture.getPixels();
                    int[] ai1 = new int[32768];
                    for (int k = 0; k < length; k++) {
                        ai1[((pixels[k] & 0xf80000) >> 9) + ((pixels[k] & 0xf800) >> 6) + ((pixels[k] & 0xf8) >> 3)]++;
                    }

                    for (int pixel = 0; pixel < pixels.length; ++pixel) {
                        if ((pixels[pixel] & 0xFFFFFF) == 0x000000) {
                            pixels[pixel] = 16711935 | 0xFF000000;
                        }
                    }

                    int[] dictionary = new int[256];
                    dictionary[0] = 0xff00ff;
                    int[] temp = new int[256];
                    for (int i1 = 0; i1 < ai1.length; i1++) {
                        int j1 = ai1[i1];
                        if (j1 > temp[255]) {
                            for (int k1 = 1; k1 < 256; k1++) {
                                if (j1 <= temp[k1]) {
                                    continue;
                                }
                                for (int i2 = 255; i2 > k1; i2--) {
                                    dictionary[i2] = dictionary[i2 - 1];
                                    temp[i2] = temp[i2 - 1];
                                }
                                dictionary[k1] = ((i1 & 0x7c00) << 9) + ((i1 & 0x3e0) << 6) + ((i1 & 0x1f) << 3) + 0x40404;
                                temp[k1] = j1;
                                break;
                            }
                        }
                        ai1[i1] = -1;
                    }
                    byte[] indices = new byte[length];
                    for (int l1 = 0; l1 < length; l1++) {
                        int j2 = pixels[l1];
                        int k2 = ((j2 & 0xf80000) >> 9) + ((j2 & 0xf800) >> 6) + ((j2 & 0xf8) >> 3);
                        int l2 = ai1[k2];
                        if (l2 == -1) {
                            int i3 = 0x3b9ac9ff;
                            int j3 = j2 >> 16 & 0xff;
                            int k3 = j2 >> 8 & 0xff;
                            int l3 = j2 & 0xff;
                            for (int i4 = 0; i4 < 256; i4++) {
                                int j4 = dictionary[i4];
                                int k4 = j4 >> 16 & 0xff;
                                int l4 = j4 >> 8 & 0xff;
                                int i5 = j4 & 0xff;
                                int j5 = (j3 - k4) * (j3 - k4) + (k3 - l4) * (k3 - l4) + (l3 - i5) * (l3 - i5);
                                if (j5 < i3) {
                                    i3 = j5;
                                    l2 = i4;
                                }
                            }

                            ai1[k2] = l2;
                        }
                        indices[l1] = (byte) l2;
                    }
                    sceneRender.loadTexture(i, dictionary, texture.getBoundWidth() / 64 - 1, indices);
                }

            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    public void run() {
        try {
            synchronized (running) {
                render();

                final List[] screen_faceArray = getScene().getScreen_faceArray();
                int[] faceOrientations = getScene().getFaceOrientations();

                int index = getMouseY() * getWidth() + getMouseX();

                if(index >= 0 && index <= screen_faceArray.length) {
                    List<Integer> hoveredFaces = screen_faceArray[getMouseY() * getWidth() + getMouseX()];
                    int hoveredFace = chooseSelectedFace(hoveredFaces);

                    int color = modelTool.getMainController().getWireframeColor();

                    for (int i = 0; i < graphicsController.pixelData.length; i++) {
                        //Face hover highlighting
                        if (hoveredFace != -1 &&
                                chooseSelectedFace(screen_faceArray[i]) == hoveredFace) {
                            graphicsController.pixelData[i] = (graphicsController.pixelData[i] & 0xFF000000) | ~(graphicsController.pixelData[i] & 0x00FFFFFF);
                        }
                        //Wireframe
                        //TODO: need a function for selecting which face to wireframe from the list
                        if (modelTool.getMainController().drawWireFrame()) {
                            if (i % width != width - 1) {
                                if (!screen_faceArray[i].equals(screen_faceArray[i + 1])) {
                                    graphicsController.pixelData[i] = color;
                                }
                            }
                            if (i % height != height - 1) {
                                int row = i % height;
                                int col = i / height;
                                int spot = row * width + col;
                                int spot2 = (row + 1) * width + col;
                                if (!screen_faceArray[spot].equals(screen_faceArray[spot2]))
                                    graphicsController.pixelData[spot] = color;
                            }
                        }
                        //Selected face
                        synchronized (selectedFace) {
                            if (selectedFace.get() != -1) {
                                int pixel1, pixel2;
                                int color2;
                                int idx;
                                if ((idx = selectedFace.get()) != -1 && faceOrientations[idx] < 0)
                                    color2 = 0xFFF90001;
                                else
                                    color2 = 0xFF00F901;

                                if (i % width < width - 1) {
                                    pixel1 = getClosestFaceToCamera(screen_faceArray[i]);
                                    pixel2 = getClosestFaceToCamera(screen_faceArray[i + 1]);
                                    if ((pixel1 == selectedFace.get() ||
                                            pixel2 == selectedFace.get()) &&
                                            pixel1 != pixel2)
                                        graphicsController.pixelData[i] = color2;
                                }
                                if (i % height < height - 1) {
                                    int row = i % height;
                                    int col = i / height;
                                    int spot = row * width + col;
                                    int spot2 = (row + 1) * width + col;
                                    pixel1 = getClosestFaceToCamera(screen_faceArray[spot]);
                                    pixel2 = getClosestFaceToCamera(screen_faceArray[spot2]);
                                    if ((pixel1 == selectedFace.get() ||
                                            pixel2 == selectedFace.get()) &&
                                            pixel1 != pixel2)
                                        graphicsController.pixelData[spot] = color2;
                                }
                            }

                        }
                    }
                }
            }

            WritableImage write = new WritableImage(width, height);
            PixelWriter pw = write.getPixelWriter();
            //XRay code
//            int[] newGuy = new int[graphicsController.pixelData.length];
//            for (int i=0; i<graphicsController.pixelData.length; ++i) {
//                int blue = 50 * screen_faceArray[i].size();
//                int color = 0xFF000000 | blue;
//                newGuy[i] = color;
//            }
            pw.setPixels(0, 0, width, height, PixelFormat.getIntArgbInstance(), graphicsController.pixelData, 0, width);

            Platform.runLater(() -> {
                viewer.setImage(write);
            });
        } catch (final Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

    private boolean intArrayContains(int[] array, int value) {
        for (int entry : array)
            if (entry == value)
                return true;

        return false;
    }

    private int getClosestFaceToCamera(List<Integer> faces) {
        return chooseSelectedFace(faces);
    }

    private int getNearestNonHiddenFace(List<Integer> faces) {
        List<Integer> hiddenFaces = getScene().getHiddenFaces();
        int index = faces.size() - 1;
        while (hiddenFaces.contains(faces.get(index))) {
            if (--index < 0)
                return index;
        }
        return faces.get(index);
    }

    private int getFarthestHiddenFace(List<Integer> faces) {
        List<Integer> hiddenFaces = getScene().getHiddenFaces();
        for (int i = 0; i < faces.size(); ++i) {
            if (hiddenFaces.contains(faces.get(i)))
                return faces.get(i);
        }

        return -1;
    }

    private int chooseSelectedFace(List<Integer> faces) {
        if (faces.isEmpty())
            return -1;

        if (!controlDown) {
            int faceID = getNearestNonHiddenFace(faces);
            if (faceID == -1)
                return -1;
            return faceID;
        } else {
            int faceID = getFarthestHiddenFace(faces);
            if (faceID == -1)
                return faces.get(faces.size() - 1);
            return faceID;
        }
    }

    public void render() {
        synchronized (getScene()) {
            clearBuffer();
            getScene().setCamera(cameraCenterX, cameraCenterY, cameraCenterZ, cameraPitch, cameraRotation, cameraYaw, cameraZoom);
            getScene().setMouseLoc(mouseX, mouseY);
            getScene().endScene(-113);
        }
    }

    public void clearBuffer() {
        Arrays.fill(getScene().getPixelData(), 0xFF000000);
    }

    public void setAnchorPoint(int x, int y) {
        this.anchorX = x;
        this.anchorY = y;
    }

    public void handleXYTranslate(int x, int y) {
//        double xDiff = (double)anchorX - x;
//        double yDiff = (double)anchorY - y;
//        camX += xDiff;
//        camY += yDiff;
//        clearBuffer();
//        sceneRender.setCamera(camX, camY, zoom, 0, 0, 0, 0);
//        sceneRender.endScene(-113);
//        render();
//        //updateCamera();
//
//        setAnchorPoint(x,y);
    }

    public void handleXYRotate(int x, int y) {
        double xDiff = (double) anchorX - x;
        double yDiff = (double) anchorY - y;
        setAnchorPoint(x, y);

        cameraPitch = 1023 & (cameraPitch + ((int) (yDiff * mouseControlSpeed)));
        cameraRotation = 1023 & (cameraRotation + ((int) (xDiff * mouseControlSpeed)));
    }

    public void handleZoom(int diff) {
        cameraZoom += diff;
    }

    public void dothething() {

        RSModel xaxis = RSModel.line(-1000, 0, 0, 1000, 0, 0, 2, Color.RED);
        RSModel yaxis = RSModel.line(0, -1000, 0, 0, 1000, 0, 2, Color.GREEN);
        RSModel zaxis = RSModel.line(0, 0, -1000, 0, 0, 1000, 2, Color.BLUE);

        //bufferModel(xaxis);
        //bufferModel(yaxis);
        //bufferModel(zaxis);
        render();
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public int getMouseX() {
        return mouseX;
    }

    public void setMouseX(int mouseX) {
        this.mouseX = mouseX;
    }

    public int getMouseY() {
        return mouseY;
    }

    public void setMouseY(int mouseY) {
        this.mouseY = mouseY;
    }

    public boolean getControlDown() {
        return this.controlDown;
    }

    public void setControlDown(boolean value) {
        this.controlDown = value;
    }

    public double getMouseControlSpeed() {
        return mouseControlSpeed;
    }

    public void setMouseControlSpeed(double mouseControlSpeed) {
        this.mouseControlSpeed = mouseControlSpeed;
    }

    public boolean isRunning() {
        return running;
    }

    public Scene getScene() {
        return this.sceneRender;
    }

    public RSModel getCurrentModel() {
        return currentModel;
    }

    public void setSelectedFace(int x, int y) {
        final List<Integer>[] screen_faceArray = getScene().getScreen_faceArray();
        int newFace = chooseSelectedFace(screen_faceArray[y * getWidth() + x]);
        if (newFace != selectedFace.get()) {
            selectedFace.set(newFace);
            modelTool.getMainController().updateFaceFillComponents();
        }
    }

    public void setCurrentModel(RSModel currentModel) {
        getScene().clearAllModels();
        getScene().getHiddenFaces().clear();
        getScene().setFaceOrientations(new int[currentModel.getFaceCount()]);

        this.currentModel = currentModel.clone();
        this.selectedFace.set(-1);
        this.cameraZoom = 400;
        this.cameraRotation = 0;
        this.cameraPitch = 0;
        this.cameraYaw = 0;

        getCurrentModel().setTranslate(0, 0, 0);
        getCurrentModel().setRotation(0, 0, 0);

        this.getCurrentModel().setDiffuseLight(56, 32, -10, -10, -10);

        sceneRender.addModel(this.getCurrentModel());
        updateUsedFillers();

        // Move the camera to be exactly inside of the object.
        getCurrentModel().resetTransformCache(7972);
        this.cameraCenterX = (getCurrentModel().getMinX() + getCurrentModel().getMaxX()) / 2;
        this.cameraCenterY = (getCurrentModel().getMinY() + getCurrentModel().getMaxY()) / 2;
        this.cameraCenterZ = (getCurrentModel().getMinZ() + getCurrentModel().getMaxZ()) / 2;

        // Update the camera zoom to fit the object within the window
        final int xLength = getCurrentModel().getMaxX() - getCurrentModel().getMinX();
        final int yLength = getCurrentModel().getMaxY() - getCurrentModel().getMinY();
        final int zLength = getCurrentModel().getMaxZ() - getCurrentModel().getMinZ();
        final int xyLongest = Math.max(xLength, yLength);
        final int maxLength = Math.max(xyLongest, zLength);
        this.cameraZoom = (int)(maxLength * 1.4D);

        // Remove fog unless the object is _very_ far away
        getScene().fogZFalloff = 1;
        getScene().fogLandscapeDistance = (getWidth() * 2 + cameraZoom * 2 - 124) * 10;
        getScene().fogEntityDistance = (getWidth() * 2 + cameraZoom * 2 - 124) * 10;
        getScene().fogSmoothingStartDistance = (getWidth() * 2 + cameraZoom * 2 - 224) * 10;
    }

    public void updateUsedFillers() {
        usedFillers.clear();
        for (int resource : currentModel.getFaceTextureFront()) {
            if (!usedFillers.contains(resource))
                usedFillers.add(resource);
        }
        for (int resource : currentModel.getFaceTextureBack()) {
            if (!usedFillers.contains(resource))
                usedFillers.add(resource);
        }
        Collections.sort(usedFillers);
    }

    public void toggleHiddenFace(int x, int y) {
        List<Integer>[] faceArray = getScene().getScreen_faceArray();
        int faceOfInterest = getClosestFaceToCamera(faceArray[y * getWidth() + x]);
        List<Integer> hiddenFaces = getScene().getHiddenFaces();
        if (hiddenFaces.contains(faceOfInterest))
            hiddenFaces.remove(Integer.valueOf(faceOfInterest));
        else
            hiddenFaces.add(faceOfInterest);
    }

    public SimpleIntegerProperty getSelectedFace() {
        return this.selectedFace;
    }

    public ObservableList<Integer> getUsedFillers() {
        return this.usedFillers;
    }
}
