package com.OpenRSC.Interface.ModelTool;

import com.OpenRSC.IO.archive.Packer;
import com.OpenRSC.IO.obj.ObjReader;
import com.OpenRSC.ModelTool;
import com.OpenRSC.Render.client.RSModel;
import com.OpenRSC.Render.client.Scene;
import com.OpenRSC.Render.client.utils.GenUtil;
import com.jfoenix.controls.*;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.util.Callback;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;

public class Controller implements Initializable {

    boolean triggerListeners = true;
    ModelTool modelTool;

    @FXML
    private AnchorPane root;

    @FXML
    private ImageView image_render;


    @FXML
    private JFXListView list_models;

    @FXML
    private JFXCheckBox check_wireframe, check_diffuse_light;

    @FXML
    private JFXRadioButton radio_front_transparent, radio_front_color, radio_front_texture, radio_back_transparent, radio_back_color, radio_back_texture;

    @FXML
    private JFXComboBox combo_front_texture, combo_back_texture;

    @FXML
    private JFXListView listview_fillers;

    @FXML
    private Label label_test;

    @FXML
    private ColorPicker colorpicker_front, colorpicker_back, colorpicker_wireframe;

    @FXML
    private JFXButton button_save, button_pack, button_import;
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        button_save.setOnMouseClicked(e -> {
            if (modelTool.getModelRenderer().getCurrentModel() != null &&
            list_models.getSelectionModel().getSelectedItem() != null) {
                list_models.getItems().set(list_models.getSelectionModel().getSelectedIndex(), modelTool.getModelRenderer().getCurrentModel());
            }
        });
        button_pack.setOnMouseClicked(e -> {
            Packer packer = new Packer();
            RSModel[] modelCache = new RSModel[list_models.getItems().size()];
            int i=0;
            for (Object object :list_models.getItems()) {
                RSModel model = (RSModel)object;
                modelCache[i++] = model;
            }
            packer.pack(modelCache);
        });
        button_import.setOnMouseClicked(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(Paths.get("").toAbsolutePath().toFile());
            FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("OBJ Files", "*.obj");
            fileChooser.getExtensionFilters().add(filter);
            File obj = fileChooser.showOpenDialog(modelTool.getPrimaryStage());
            if (obj == null)
                return;
            ObjReader objReader = new ObjReader();
            RSModel importedModel = objReader.objToModel(obj);
            TextInputDialog textInputDialog = new TextInputDialog();
            textInputDialog.setHeaderText("Please enter a name for the object");
            textInputDialog.showAndWait();
            String name = textInputDialog.getResult();

            int wantedHash = 0;
            for (int k = 0; k < name.length(); k++)
                wantedHash = (wantedHash * 61 + name.charAt(k)) - 32;

            importedModel.setName(name);
            importedModel.setHash(wantedHash);
            list_models.getItems().add(importedModel);
        });
        colorpicker_front.valueProperty().addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observableValue, Color color, Color t1) {
                if (!triggerListeners)
                    return;
                int red = (int) ((t1.getRed() * 255) / 8);
                int green = (int) ((t1.getGreen() * 255) / 8);
                int blue = (int) ((t1.getBlue() * 255) / 8);
                red <<= 3;
                green <<= 3;
                blue <<= 3;
                Color c = Color.rgb(red, green, blue);
                if (!c.equals(t1)) {
                    colorpicker_front.setValue(c);
                } else if (radio_front_color.isSelected()) {
                    modelTool.getModelRenderer().getCurrentModel().getFaceTextureFront()[modelTool.getModelRenderer().getSelectedFace().get()] = GenUtil.colorToResource(red, green, blue);
                    modelTool.getModelRenderer().updateUsedFillers();
                }
            }
        });

        colorpicker_back.valueProperty().addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observableValue, Color color, Color t1) {
                if (!triggerListeners)
                    return;
                int red = (int) ((t1.getRed() * 255) / 8);
                int green = (int) ((t1.getGreen() * 255) / 8);
                int blue = (int) ((t1.getBlue() * 255) / 8);
                red <<= 3;
                green <<= 3;
                blue <<= 3;
                Color c = Color.rgb(red, green, blue);
                if (!c.equals(t1)) {
                    colorpicker_back.setValue(c);
                } else if (radio_back_color.isSelected()) {
                    modelTool.getModelRenderer().getCurrentModel().getFaceTextureBack()[modelTool.getModelRenderer().getSelectedFace().get()] = GenUtil.colorToResource(red, green, blue);
                    modelTool.getModelRenderer().updateUsedFillers();
                }

            }
        });

        radio_front_transparent.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if (!triggerListeners)
                    return;
                if (t1) {
                    int faceID = modelTool.getModelRenderer().getSelectedFace().get();

                    modelTool.getModelRenderer().getCurrentModel().getFaceTextureFront()[faceID] = Scene.TRANSPARENT;
                    modelTool.getModelRenderer().updateUsedFillers();
                }
            }
        });

        radio_front_color.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if (!triggerListeners)
                    return;
                int red = (int) ((colorpicker_front.getValue().getRed() * 255) / 8);
                int green = (int) ((colorpicker_front.getValue().getGreen() * 255) / 8);
                int blue = (int) ((colorpicker_front.getValue().getBlue() * 255) / 8);
                red <<= 3;
                green <<= 3;
                blue <<= 3;
                modelTool.getModelRenderer().getCurrentModel().getFaceTextureFront()[modelTool.getModelRenderer().getSelectedFace().get()] = GenUtil.colorToResource(red, green, blue);
                modelTool.getModelRenderer().updateUsedFillers();
            }
        });

        radio_front_texture.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if (!triggerListeners)
                    return;
                if (combo_front_texture.getSelectionModel().getSelectedItem() == null) {
                    combo_front_texture.getSelectionModel().select(0);
                }

                modelTool.getModelRenderer().getCurrentModel().getFaceTextureFront()[modelTool.getModelRenderer().getSelectedFace().get()] = Integer.parseInt(((ModelTool.TextureEntry) (combo_front_texture.getSelectionModel().getSelectedItem())).getName());
                modelTool.getModelRenderer().updateUsedFillers();
            }
        });

        radio_back_transparent.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if (!triggerListeners)
                    return;
                int faceID = modelTool.getModelRenderer().getSelectedFace().get();
                if (faceID != -1) {
                    if (t1) {
                        modelTool.getModelRenderer().getCurrentModel().getFaceTextureBack()[faceID] = Scene.TRANSPARENT;
                        modelTool.getModelRenderer().updateUsedFillers();
                    }

                }

            }
        });

        radio_back_color.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if (!triggerListeners)
                    return;
                int red = (int) ((colorpicker_back.getValue().getRed() * 255) / 8);
                int green = (int) ((colorpicker_back.getValue().getGreen() * 255) / 8);
                int blue = (int) ((colorpicker_back.getValue().getBlue() * 255) / 8);
                red <<= 3;
                green <<= 3;
                blue <<= 3;
                modelTool.getModelRenderer().getCurrentModel().getFaceTextureBack()[modelTool.getModelRenderer().getSelectedFace().get()] = GenUtil.colorToResource(red, green, blue);
                modelTool.getModelRenderer().updateUsedFillers();
            }
        });

        radio_back_texture.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if (!triggerListeners)
                    return;
                if (combo_back_texture.getSelectionModel().getSelectedItem() == null) {
                    combo_back_texture.getSelectionModel().select(0);
                }

                modelTool.getModelRenderer().getCurrentModel().getFaceTextureBack()[modelTool.getModelRenderer().getSelectedFace().get()] = Integer.parseInt(((ModelTool.TextureEntry) (combo_back_texture.getSelectionModel().getSelectedItem())).getName());
                modelTool.getModelRenderer().updateUsedFillers();
            }
        });

        image_render.setOnMousePressed(e -> {
            switch (e.getButton()) {
                case PRIMARY:
                    modelTool.getModelRenderer().setSelectedFace((int) e.getX(), (int) e.getY());
                    break;
                case SECONDARY:
                    if (e.isShiftDown()) {
                        modelTool.getModelRenderer().toggleHiddenFace((int) e.getX(), (int) e.getY());
                    } else {
                        modelTool.getModelRenderer().setAnchorPoint((int) e.getX(), (int) e.getY());
                    }
                    break;
            }
        });

        image_render.setOnMouseDragged(e -> {
            int x = (int) e.getX();
            int y = (int) e.getY();
            switch (e.getButton()) {
                case PRIMARY:
                    modelTool.getModelRenderer().handleXYTranslate(x, y);
                    break;
                case SECONDARY:
                    if (!e.isShiftDown())
                        modelTool.getModelRenderer().handleXYRotate(x, y);
                    break;
            }
        });

        image_render.setOnMouseMoved(e -> {
            int x = (int) e.getX();
            int y = (int) e.getY();
            modelTool.getModelRenderer().setMouseX(x);
            modelTool.getModelRenderer().setMouseY(y);
        });

        root.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.CONTROL)
                modelTool.getModelRenderer().setControlDown(true);
        });
        root.setOnKeyReleased(e -> {
            if (e.getCode() == KeyCode.CONTROL)
                modelTool.getModelRenderer().setControlDown(false);
        });

        image_render.setOnScroll(e -> {
            modelTool.getModelRenderer().handleZoom(-(int) e.getDeltaY());
        });

        list_models.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object o, Object t1) {
                modelTool.getModelRenderer().setCurrentModel((RSModel) t1);
            }
        });

        check_diffuse_light.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                int faceID = modelTool.getModelRenderer().getSelectedFace().get();
                if (faceID != -1) {
                    modelTool.getModelRenderer().getCurrentModel().getFaceDiffuseLight()[faceID] = t1 ? Scene.TRANSPARENT : 0;
                    modelTool.getModelRenderer().updateUsedFillers();
                }
            }
        });

        combo_front_texture.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object o, Object t1) {
                if (t1 != null && radio_front_texture.isSelected()) {
                    modelTool.getModelRenderer().getCurrentModel().getFaceTextureFront()[modelTool.getModelRenderer().getSelectedFace().get()] = Integer.parseInt(((ModelTool.TextureEntry) (combo_front_texture.getSelectionModel().getSelectedItem())).getName());
                    modelTool.getModelRenderer().updateUsedFillers();
                }
            }
        });

        combo_back_texture.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object o, Object t1) {
                if (t1 != null && radio_back_texture.isSelected()) {
                    modelTool.getModelRenderer().getCurrentModel().getFaceTextureBack()[modelTool.getModelRenderer().getSelectedFace().get()] = Integer.parseInt(((ModelTool.TextureEntry) (combo_back_texture.getSelectionModel().getSelectedItem())).getName());
                    modelTool.getModelRenderer().updateUsedFillers();
                }
            }
        });

        ToggleGroup frontGroup = new ToggleGroup();
        ToggleGroup backGroup = new ToggleGroup();
        radio_front_transparent.setToggleGroup(frontGroup);
        radio_front_color.setToggleGroup(frontGroup);
        radio_front_texture.setToggleGroup(frontGroup);
        radio_back_transparent.setToggleGroup(backGroup);
        radio_back_color.setToggleGroup(backGroup);
        radio_back_texture.setToggleGroup(backGroup);
        initCellRenderers();
    }

    private void initCellRenderers() {
        triggerListeners = false;
        Callback<ListView<ModelTool.TextureEntry>, ListCell<ModelTool.TextureEntry>> cellFactory1 = new Callback<>() {

            @Override
            public ListCell<ModelTool.TextureEntry> call(ListView<ModelTool.TextureEntry> l) {
                return new ListCell<>() {

                    @Override
                    protected void updateItem(ModelTool.TextureEntry entry, boolean empty) {
                        super.updateItem(entry, empty);
                        if (entry == null || empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(entry.getPreview());
                            setText(entry.getName());
                            setPadding(new Insets(1));
                        }
                    }
                };
            }
        };
        ListCell listCell1 = new ListCell<ModelTool.TextureEntry>() {
            @Override
            protected void updateItem(ModelTool.TextureEntry entry, boolean btl) {
                super.updateItem(entry, btl);
                if (entry != null) {
                    ImageView iv = new ImageView(entry.getPreview().getImage());
                    iv.setPreserveRatio(true);
                    iv.setFitWidth(32);
                    setGraphic(iv);
                    setText(entry.getName());
                }
            }
        };

        Callback<ListView<ModelTool.TextureEntry>, ListCell<ModelTool.TextureEntry>> cellFactory2 = new Callback<>() {

            @Override
            public ListCell<ModelTool.TextureEntry> call(ListView<ModelTool.TextureEntry> l) {
                return new ListCell<>() {

                    @Override
                    protected void updateItem(ModelTool.TextureEntry entry, boolean empty) {
                        super.updateItem(entry, empty);
                        if (entry == null || empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(entry.getPreview());
                            setText(entry.getName());
                            setPadding(new Insets(1));
                        }
                    }
                };
            }
        };
        ListCell listCell2 = new ListCell<ModelTool.TextureEntry>() {
            @Override
            protected void updateItem(ModelTool.TextureEntry entry, boolean btl) {
                super.updateItem(entry, btl);
                if (entry != null) {
                    ImageView iv = new ImageView(entry.getPreview().getImage());
                    iv.setPreserveRatio(true);
                    iv.setFitWidth(32);
                    setGraphic(iv);
                    setText(entry.getName());
                }
            }
        };

        Callback<ListView<Integer>, ListCell<Integer>> cellFactory3 = new Callback<>() {

            @Override
            public ListCell<Integer> call(ListView<Integer> l) {
                return new ListCell<>() {

                    @Override
                    protected void updateItem(Integer number, boolean empty) {
                        super.updateItem(number, empty);
                        if (number == null || empty) {
                            setText("");
                            setGraphic(null);
                        } else {
                            if (number < 0) {
                                int[] colorPreview = new int[1024];
                                int color = GenUtil.resourceToColor(number);
                                Arrays.fill(colorPreview, color);
                                WritableImage write = new WritableImage(32, 32);
                                PixelWriter pw = write.getPixelWriter();
                                pw.setPixels(0, 0, 32, 32, PixelFormat.getIntArgbInstance(), colorPreview, 0, 32);
                                setText(String.format("Color: #%06X", color & 0xFFFFFF));
                                setGraphic(new ImageView(write));
                            } else if (number == Scene.TRANSPARENT)
                                setText("Transparent");
                            else {
                                setText("Texture: " + number);
                                ModelTool.TextureEntry texture = ((ModelTool.TextureEntry) combo_front_texture.getItems().get(number)).clone();
                                setGraphic(texture.getPreview());
                            }
                            setPadding(new Insets(1));
                        }
                    }
                };
            }
        };
        combo_front_texture.setButtonCell(listCell1);
        combo_front_texture.setCellFactory(cellFactory1);
        combo_back_texture.setButtonCell(listCell2);
        combo_back_texture.setCellFactory(cellFactory2);
        listview_fillers.setCellFactory(cellFactory3);
        triggerListeners = true;
    }

    public void setModelTool(ModelTool modelTool) {
        this.modelTool = modelTool;
    }

    public ImageView getViewer() {
        return this.image_render;
    }

    public void listModels(RSModel[] models) {
        List<RSModel> modelList = Arrays.asList(models);
        Comparator<RSModel> someFieldComparator = new Comparator<RSModel>() {
            @Override
            public int compare(RSModel m1, RSModel m2) {
                return m1.getName().compareTo(m2.getName());
            }
        };
        java.util.Collections.sort(modelList, someFieldComparator);
        list_models.getItems().addAll(models);

    }

    public void clearFaceFillComponents() {
//        check_diffuse_light.setSelected(false);
//
//        combo_front_texture.getSelectionModel().select(null);
//        combo_back_texture.getSelectionModel().select(null);
    }

    public void createBindings() {
        BooleanBinding disabled = modelTool.getModelRenderer().getSelectedFace().isEqualTo(-1);
        radio_front_transparent.disableProperty().bind(disabled);
        radio_front_color.disableProperty().bind(disabled);
        radio_front_texture.disableProperty().bind(disabled);
        radio_back_transparent.disableProperty().bind(disabled);
        radio_back_color.disableProperty().bind(disabled);
        radio_back_texture.disableProperty().bind(disabled);
        combo_front_texture.disableProperty().bind(disabled);
        combo_back_texture.disableProperty().bind(disabled);
    }

    public void updateFaceFillComponents() {
        triggerListeners = false;
        modelTool.getMainController().clearFaceFillComponents();
        int selectedFace = modelTool.getModelRenderer().getSelectedFace().get();
        RSModel currentModel = modelTool.getModelRenderer().getCurrentModel();
        if (selectedFace != -1) {
            int faceTextureFront = currentModel.getFaceTextureFront()[selectedFace];
            int faceTextureBack = currentModel.getFaceTextureBack()[selectedFace];
            if (faceTextureFront == Scene.TRANSPARENT) {
                radio_front_transparent.setSelected(true);
            } else if (faceTextureFront >= 0) {
                radio_front_texture.setSelected(true);
                combo_front_texture.getSelectionModel().select(faceTextureFront);
            } else {
                int resource = -(faceTextureFront + 1);
                int var3 = (resource & 0x7C00) >> 10;
                int var4 = (0x3E0 & resource) >> 5;
                int var5 = 0x1F & resource;
                Color c = Color.rgb(var3 << 3, var4 << 3, var5 << 3);

                radio_front_color.setSelected(true);
                colorpicker_front.setValue(c);
            }
            if (faceTextureBack == Scene.TRANSPARENT) {
                radio_back_transparent.setSelected(true);
            } else if (faceTextureBack >= 0) {
                radio_back_texture.setSelected(true);
                combo_back_texture.getSelectionModel().select(faceTextureBack);
            } else {
                int resource = -(faceTextureBack + 1);
                int var3 = (resource & 0x7C00) >> 10;
                int var4 = (0x3E0 & resource) >> 5;
                int var5 = 0x1F & resource;
                Color c = Color.rgb(var3 << 3, var4 << 3, var5 << 3);

                radio_back_color.setSelected(true);
                colorpicker_back.setValue(c);
            }
            label_test.setText(currentModel.getFaceDiffuseLight()[selectedFace] + "");
//            if (currentModel.getFaceDiffuseLight()[selectedFace] == Scene.TRANSPARENT)
//                check_diffuse_light.setSelected(true);
//            else
//                check_diffuse_light.setSelected(false);
        }
        triggerListeners = true;
    }

    public void addAvailableTexture(ModelTool.TextureEntry textureEntry) {
        combo_front_texture.getItems().add(textureEntry);
        combo_back_texture.getItems().add(textureEntry.clone());
    }

    public boolean drawWireFrame() {
        return this.check_wireframe.isSelected();
    }

    public int getWireframeColor() {
        int red = (int)(colorpicker_wireframe.getValue().getRed() * 255);
        int green = (int)(colorpicker_wireframe.getValue().getGreen() * 255);
        int blue = (int)(colorpicker_wireframe.getValue().getBlue() * 255);
        return 0xFF000000 | (red << 16) + (green << 8) + blue;
    }

    public void linkFillList(ObservableList<Integer> list) {
        listview_fillers.setItems(list);
    }
}
