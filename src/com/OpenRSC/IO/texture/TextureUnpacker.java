package com.OpenRSC.IO.texture;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.zip.GZIPInputStream;

import com.OpenRSC.IO.texture.Entry.TYPE;
import com.OpenRSC.IO.texture.Frame.LAYER;
import org.apache.commons.io.FilenameUtils;

public class TextureUnpacker {

    public TextureUnpacker() {

    }

    public Entry unpackEntry(File file) {
        if (!file.exists())
            return null;

        try {
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {
                GZIPInputStream in = new GZIPInputStream(fis);
                byte[] buffer = new byte[65536];
                int noRead;
                while ((noRead = in.read(buffer)) != -1) {
                    out.write(buffer, 0, noRead);
                }

                in.close();
            } finally {
                try { out.close(); fis.close();} catch (Exception e) {}
            }

            ByteBuffer input = ByteBuffer.wrap(out.toByteArray());
            TYPE type;

            Entry newEntry = new Entry(
                    FilenameUtils.removeExtension(file.getName()),
                    type = TYPE.get((int) input.get() & 0xFF),
                    type.getLayers().length == 0 ? null : LAYER.get((int) input.get() & 0xFF),
                    (int) input.get() & 0xFF
            );

            readEntry(input, newEntry);

            return newEntry;

        } catch (IOException a) {
            a.printStackTrace();
            return null;
        }
    }

    private void readEntry(ByteBuffer stream, Entry entry) {
        try {
            int tableSize = stream.get() & 0xFF;
            int[] colorTable = new int[++tableSize];

            for (int i = 0; i < colorTable.length; ++i) {
                int Red = stream.get() & 0xFF;
                int Green = stream.get() & 0xFF;
                int Blue = stream.get() & 0xFF;
                colorTable[i] = Red << 16 | Green << 8 | Blue;
            }

            for (int i = 0; i < entry.getFrames().length; ++i) {
                Frame frame = new Frame(
                        (int) stream.getShort() & 0xFFFF,
                        (int) stream.getShort() & 0xFFFF,
                        stream.get() == 1,
                        (int) stream.getShort(),
                        (int) stream.getShort(),
                        (int) stream.getShort() & 0xFFFF,
                        (int) stream.getShort() & 0xFFFF
                );

                for (int p = 0; p < frame.getPixels().length; ++p)
                    frame.getPixels()[p] = colorTable[(int) stream.get() & 0xFF];

                entry.getFrames()[i] = frame;
            }
        } catch (Exception a) { a.printStackTrace(); }
    }

    private String readString(ByteBuffer stream) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            int character;
            while ((character = stream.get()) != 0)
                stringBuilder.append((char)(character & 0xFF));
        } catch (Exception a) {
            a.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
