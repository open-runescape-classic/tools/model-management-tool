package com.OpenRSC.IO.obj;

import com.OpenRSC.Render.client.RSModel;
import com.OpenRSC.Render.client.Scene;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;

public class ObjReader {
    private static final int color_newImport = -12894;
    public RSModel objToModel(File objFile) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(objFile));

            boolean readFailed = false;
            String st;
            Scanner scanner = null;
            Stack<Float> stack = new Stack<Float>();
            Stack<Integer> faceVertexStack = new Stack<Integer>();
            Stack<Integer> faceIndecesStack = new Stack<Integer>();
            int vertexCount=0, faceCount=0, temp=0;
            int faceIndices[][];

            while ((st = br.readLine()) != null)
            {
                //System.out.println(st);
                if (st.charAt(1) != ' ') {
                    continue;
                }
                scanner = new Scanner(st);
                scanner.next();
                switch (st.charAt(0))
                {
                    case 'V':
                    case 'v':
                        vertexCount++;
                        temp = stack.size();

                        while (scanner.hasNextFloat())
                            stack.push(scanner.nextFloat());
                        if (stack.size() != temp + 3) {
                            System.out.println("Unrecognized obj format");
                            readFailed = true;
                            break;
                        }
                        break;
                    case 'F':
                    case 'f':
                        int counter = 0;
                        faceCount++;
                        if (st.contains("/"))
                            st = st;
                        while (scanner.hasNext()){
                            String next = scanner.next();
                            if (next.contains("/"))
                                next = next.substring(0,next.indexOf('/'));
                            counter++;
                            faceVertexStack.push(Integer.parseInt(next));
                        }
                        faceIndecesStack.push(counter);
                        break;
                    default:
                        continue;
                }
            }
            br.close();
            if (!readFailed){
                int vertX[] = new int[vertexCount];
                int vertY[] = new int[vertexCount];
                int vertZ[] = new int[vertexCount];
                int faceIndexCount[] = new int[faceCount];
                int faceTextureFront[] = new int[faceCount];
                int faceTextureBack[] = new int[faceCount];
                int faceDiffuseLight[] = new int[faceCount];

                for (int p = 0; p < faceCount; p++)
                {
                    faceTextureFront[p] = color_newImport;
                    faceTextureBack[p] = color_newImport;
                    faceDiffuseLight[p] = Scene.TRANSPARENT;
                }
                faceIndices = new int[faceCount][];
                int k=0;
                for (k = vertexCount-1; k >= 0; k--)
                {
                    vertZ[k] = stack.pop().intValue();
                    vertY[k] = stack.pop().intValue();
                    vertX[k] = stack.pop().intValue();
                }
                int z = 0;
                for (k = faceCount-1; k>=0; k--) {
                    faceIndexCount[k] = (int) faceIndecesStack.pop();
                    faceIndices[k] = new int[faceIndexCount[k]];
                    for (z = faceIndexCount[k]-1; z >= 0; z--)
                    {
                        faceIndices[k][z] = (int) faceVertexStack.pop()-1;
                    }
                }

                RSModel model = new RSModel(vertexCount, faceCount, vertX, vertY, vertZ, faceIndexCount,
                        faceTextureFront, faceTextureBack, faceDiffuseLight, faceIndices);

                //if (!model.modelUpdateInfo(objFile.substring(0, objFile.length() - 3) + "inf"+'o'))
                //	return null;
                return model;
            }

        }
        catch (IOException a)
        {
            System.err.print("Something went wrong");
        }
        return null;
    }
}
