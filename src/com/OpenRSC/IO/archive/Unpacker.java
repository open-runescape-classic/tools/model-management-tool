package com.OpenRSC.IO.archive;

import com.OpenRSC.IO.archive.data.DataFileDecrypter;
import com.OpenRSC.IO.archive.data.DataOperations;
import com.OpenRSC.Render.client.RSModel;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;

public class Unpacker {

    private RSModel[] modelCache;

    public Unpacker() {}

    public RSModel[] unpack() {
        File modelFile = new File("res/models.orsc");
        byte[] models = unpackData(modelFile.toString());
        int numEntries = DataOperations.getUnsigned2Bytes(models, 0);
        modelCache = new RSModel[numEntries];
        for (int j = 0; j < numEntries; j++) {
            entryInfo entry = DataOperations.getIndexOffset(j, models);
            if (entry.offset == 0) {
                modelCache[j] = new RSModel(1, 1);
                modelCache[j].setName("zero_offset");
            } else {
                modelCache[j] = new RSModel(models, entry.offset, true);
                modelCache[j].setName(DataOperations.hashLookup(entry.hash));
            }

            modelCache[j].m_cb = modelCache[j].getName().equalsIgnoreCase("giantcrystal");
            modelCache[j].setHash(entry.hash);
        }
        return modelCache;
    }
    public byte[] unpackData(String filename) {
        int decmp_len = 0;
        int cmp_len = 0;
        byte[] data = null;
        try {
            java.io.InputStream inputstream = DataOperations.streamFromPath(filename);
            DataInputStream datainputstream = new DataInputStream(inputstream);
            byte[] headers = new byte[6];
            datainputstream.readFully(headers, 0, 6);
            decmp_len = ((headers[0] & 0xFF) << 16) + ((headers[1] & 0xFF) << 8) + (headers[2] & 0xFF);
            cmp_len = ((headers[3] & 0xFF) << 16) + ((headers[4] & 0xFF) << 8) + (headers[5] & 0xFF);
            int l = 0;
            data = new byte[cmp_len];
            while (l < cmp_len) {
                int i1 = cmp_len - l;
                if (i1 > 1000)
                    i1 = 1000;
                datainputstream.readFully(data, l, i1);
                l += i1;
            }
            datainputstream.close();
        } catch (IOException _ex) {
            _ex.printStackTrace();
        }
        if (cmp_len != decmp_len) {
            byte[] buffer = new byte[decmp_len];
            DataFileDecrypter.unpackData(buffer, decmp_len, data, cmp_len, 0);
            return buffer;
        }
        return data;
    }

    public static class entryInfo{
        public int hash;
        public int offset;
        public entryInfo(int hash, int offset){
            this.hash = hash;
            this.offset = offset;
        }
    }
}
