package com.OpenRSC.IO.archive;

import com.OpenRSC.IO.archive.data.DataOperations;
import com.OpenRSC.Render.client.RSModel;
import com.OpenRSC.Render.client.utils.FastMath;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;

public class Packer {
    public void packModel(DataOutputStream dOS, RSModel model) {
        try {
            dOS.writeByte((model.getVertexCount() >> 8) & 255);
            dOS.writeByte(model.getVertexCount() & 255);
            dOS.writeByte((model.getFaceCount() >> 8) & 255);
            dOS.writeByte(model.getFaceCount() & 255);
            //Write all X coordinates
            for (int y = 0; y < model.getVertexCount(); y++) {
                dOS.writeByte((model.getVertexX(y) >> 8) & 0xFF);
                dOS.writeByte(model.getVertexX(y) & 0xFF);
            }
            //Write all Y coordinates
            for (int y = 0; y < model.getVertexCount(); y++) {
                dOS.writeByte((model.getVertexY(y) >> 8) & 0xFF);
                dOS.writeByte(model.getVertexY(y) & 0xFF);
            }
            //Write all Z coordinates
            for (int y = 0; y < model.getVertexCount(); y++) {
                dOS.writeByte((model.getVertexZ(y) >> 8) & 0xFF);
                dOS.writeByte(model.getVertexZ(y) & 0xFF);
            }
            //Write face index count for each face
            for (int y = 0; y < model.getFaceCount(); y++) {
                dOS.writeByte(model.getFaceIndexCount(y) & 0xFF);
            }

            //Write face texture front
            int textureToWrite = 0;
            for (int y = 0; y < model.getFaceCount(); y++) {
                textureToWrite = model.getFaceTextureFront()[y];
                if (textureToWrite == model.getM_Vb())
                    textureToWrite = 0x7FFF;

                dOS.writeByte((textureToWrite >> 8) & 0xFF);
                dOS.writeByte(textureToWrite & 0xFF);
            }
            //Write face texture back
            for (int y = 0; y < model.getFaceCount(); y++) {
                textureToWrite = model.getFaceTextureBack()[y];
                if (textureToWrite == model.getM_Vb())
                    textureToWrite = 0x7FFF;

                dOS.writeByte((textureToWrite >> 8) & 0xFF);
                dOS.writeByte(textureToWrite & 0xFF);
            }
            //Write face diffuse light
            int diffuseLight = 0;
            for (int y = 0; y < model.getFaceCount(); y++) {
                diffuseLight = model.getFaceDiffuseLight()[y];
                if (diffuseLight != 0)
                    diffuseLight = 0x1;
                dOS.writeByte(diffuseLight & 0xFF);
            }

            //Write primitive indices
            for (int y = 0; y < model.getFaceCount(); y++) {
                for (int k = 0; k < model.getFacePointCount(y); k++) {
                    if (model.getVertexCount() < 256)
                        dOS.writeByte(model.getFacePoints(y, k) & 0xFF);
                    else {
                        dOS.writeByte((model.getFacePoints(y, k) >> 8) & 255);
                        dOS.writeByte(model.getFacePoints(y, k) & 255);
                    }
                }
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    public void pack(RSModel[] modelCache) {
        try {
            File file = new File("res/models.orsc");
            FileOutputStream fOS = new FileOutputStream(file);
            DataOutputStream dOS = new DataOutputStream(fOS);

            //Write header for compression information (6)
            for (int i = 0; i < 6; ++i)
                dOS.writeByte(1);

            //Write the number of entries in the model cache (2)
            dOS.writeByte((modelCache.length >> 8) & 0xFF);
            dOS.writeByte(modelCache.length & 0xFF);

            //Header for entry hashes and filesizes (10 each)
            for (RSModel model : modelCache) {
                int hash = model.getHash();
                int fileSize = DataOperations.getModelFileSize(model);

                dOS.writeByte((hash >> 24) & 0xFF);
                dOS.writeByte((hash >> 16) & 0xFF);
                dOS.writeByte((hash >> 8) & 0xFF);
                dOS.writeByte((hash) & 0xFF);
                dOS.writeByte((fileSize >> 16) & 0xFF);
                dOS.writeByte((fileSize >> 8) & 0xFF);
                dOS.writeByte((fileSize) & 0xFF);
                dOS.writeByte((fileSize >> 16) & 0xFF);
                dOS.writeByte((fileSize >> 8) & 0xFF);
                dOS.writeByte((fileSize) & 0xFF);
            }

            for (RSModel model : modelCache)
                packModel(dOS, model);

            dOS.close();
            fOS.close();

            //Update the header of the file
            //TODO: compression
            file = new File("res/models.orsc");
            long totalSize = file.length() - 6;
            RandomAccessFile accessor = new RandomAccessFile(file, "rws");

            accessor.seek(0);
            accessor.writeByte(FastMath.bitwiseAnd((int) (totalSize >> 16), 0xFF));
            accessor.writeByte(FastMath.bitwiseAnd((int) (totalSize >> 8), 0xFF));
            accessor.writeByte(FastMath.bitwiseAnd((int) totalSize, 0xFF));
            accessor.writeByte(FastMath.bitwiseAnd((int) (totalSize >> 16), 0xFF));
            accessor.writeByte(FastMath.bitwiseAnd((int) (totalSize >> 8), 0xFF));
            accessor.writeByte(FastMath.bitwiseAnd((int) totalSize, 0xFF));
            accessor.close();
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

}
