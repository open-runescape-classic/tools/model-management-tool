package com.OpenRSC.IO.archive.data;

import com.OpenRSC.IO.archive.Unpacker;
import com.OpenRSC.Render.client.RSModel;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class DataOperations {
    private static Map<Integer, String> modelHashTable = new HashMap<>();
    private static final char[] special_characters = "~`!@#$%^&*()_-+={}[]|'\";:?><,./".toCharArray();

    private static int baseLengthArray[] = {0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095, 8191, 16383,
            32767, 65535, 0x1ffff, 0x3ffff, 0x7ffff, 0xfffff, 0x1fffff, 0x3fffff, 0x7fffff, 0xffffff, 0x1ffffff,
            0x3ffffff, 0x7ffffff, 0xfffffff, 0x1fffffff, 0x3fffffff, 0x7fffffff, -1};

    static {
        Arrays.sort(special_characters);
        modelHashTable.put(-1228854587, "WATERFALLLEV2.OB3");
        modelHashTable.put(1727821562, "LOGBRIDGE LEV2.OB3");
        modelHashTable.put(648642223, "SPIKEDPIT.OB3");
        modelHashTable.put(-1094749520, "WALLPOST.OB3");
        modelHashTable.put(-1824803030, "GEMROCK.OB3");
        modelHashTable.put(483364060, "CAVE TUBETRAP.OB3");
        modelHashTable.put(-163623380, "LOGBRIDGECURVEDLOW.OB3");
        modelHashTable.put(-1776820721, "1-1DARK.OB3");
        modelHashTable.put(85765380, "GOLDROCK1.OB3");
        modelHashTable.put(-379345544, "OGRE STANDARD.OB3");
        modelHashTable.put(713666199, "COFFIN.OB3");
        modelHashTable.put(763644315, "VINEPLANT.OB3");
        modelHashTable.put(377978531, "MILLTOP.OB3");
        modelHashTable.put(-504535588, "FALLENTREE.OB3");
        modelHashTable.put(1140413386, "DOUBLEDOORSOPEN.OB3");
        modelHashTable.put(-1360402723, "GNOMEGLIDERCRASHED.OB3");
        modelHashTable.put(367112667, "BULLRUSHES.OB3");
        modelHashTable.put(875294743, "ANIMALSKULL.OB3");
        modelHashTable.put(1631308038, "SHIPSPRAY1.OB3");
        modelHashTable.put(1168768406, "CARCASS.OB3");
        modelHashTable.put(49195729, "CLOCKPOLERED.OB3");
        modelHashTable.put(-367075747, "SANDPIT.OB3");
        modelHashTable.put(-601589657, "TOTEMTREE4.OB3");
        modelHashTable.put(1776348583, "GIANTCRYSTAL.OB3");
        modelHashTable.put(-615435498, "TOTEMTREE3.OB3");
        modelHashTable.put(361857499, "CAVE LEVER.OB3");
        modelHashTable.put(81999016, "TORCHA1.OB3");
        modelHashTable.put(1816272911, "RANGE.OB3");
        modelHashTable.put(1634453796, "MANHOLEOPEN.OB3");
        modelHashTable.put(95844857, "TORCHA2.OB3");
        modelHashTable.put(1253843689, "TREEPLATFORMHIGH2.OB3");
        modelHashTable.put(1645153879, "SHIPSPRAY2.OB3");
        modelHashTable.put(-1904180519, "MARBLEPILLAR.OB3");
        modelHashTable.put(1523838601, "LARGEBONE.OB3");
        modelHashTable.put(-1392635742, "FLOORWEB.OB3");
        modelHashTable.put(-619928750, "ROCKPOOLWATER.OB3");
        modelHashTable.put(1956318458, "LOG_BALANCE1.OB3");
        modelHashTable.put(-1574721983, "CAVE BLOODWELL.OB3");
        modelHashTable.put(44160707, "MILLBASE.OB3");
        modelHashTable.put(1725010615, "ADAMITEROCK1.OB3");
        modelHashTable.put(1637137907, "BRIDGE SECTION COLLAPSED.OB3");
        modelHashTable.put(2040485356, "WILLOWTREE.OB3");
        modelHashTable.put(1227393374, "SANDPILE.OB3");
        modelHashTable.put(-936850337, "CAVE TUBETRAPA ROPE.OB3");
        modelHashTable.put(-1141135488, "_515450526");
        modelHashTable.put(-2091399380, "CAVE SPEARTRAPA.OB3");
        modelHashTable.put(518786711, "FOODTROUGH.OB3");
        modelHashTable.put(-1108600432, "TOTEMTREEROTTEN5.OB3");
        modelHashTable.put(-391327534, "SMASHEDCHAIR.OB3");
        modelHashTable.put(462583789, "CAVE PLATFORM VERYSMALL.OB3");
        modelHashTable.put(882606108, "_3074233605");
        modelHashTable.put(306632399, "MAGEARENA COLOMN.OB3");
        modelHashTable.put(-318056063, "DEADTREE1.OB3");
        modelHashTable.put(1906856229, "CAVE PLATFORM SMALL2.OB3");
        modelHashTable.put(-2010167135, "FLOWER.OB3");
        modelHashTable.put(1831762336, "MAGEARENA TALLWALL.OB3");
        modelHashTable.put(112106047, "TREEPLATFORMLOW2.OB3");
        modelHashTable.put(206628040, "PIPE&DRAIN.OB3");
        modelHashTable.put(-1242700428, "WATERFALLLEV1.OB3");
        modelHashTable.put(-1652640283, "OAKTREE.OB3");
        modelHashTable.put(1577838231, "SHIPLEAK.OB3");
        modelHashTable.put(1480996070, "GRAVESTONE2.OB3");
        modelHashTable.put(1876385651, "ROCKPOOL.OB3");
        modelHashTable.put(1649212137, "SHAMANCAVE.OB3");
        modelHashTable.put(866359044, "_1540002513");
        modelHashTable.put(271219844, "_529296367");
        modelHashTable.put(1489432680, "ARDOUNGEWALLCORNER.OB3");
        modelHashTable.put(378475041, "CAVEENTRANCE.OB3");
        modelHashTable.put(-531201818, "PALM2.OB3");
        modelHashTable.put(-1557879908, "STALAGMITES.OB3");
        modelHashTable.put(1297382890, "COUNTER.OB3");
        modelHashTable.put(-1165024689, "CAVE SPEARTRAP.OB3");
        modelHashTable.put(-1377032918, "BUSH2.OB3");
        modelHashTable.put(-951087224, "TOTEMTREEGOOD.OB3");
        modelHashTable.put(-669716258, "WATERSQUARE.OB3");
        modelHashTable.put(1628622303, "GNOMEGOAL.OB3");
        modelHashTable.put(-280585335, "TREEPLATFORMLOW.OB3");
        modelHashTable.put(1480616081, "MADMACHINE.OB3");
        modelHashTable.put(-1204642591, "SPINNINGWHEEL.OB3");
        modelHashTable.put(-989133347, "GNOMEFENCE.OB3");
        modelHashTable.put(-655844289, "MAGICTREE.OB3");
        modelHashTable.put(368305449, "STONESTAIRS.OB3");
        modelHashTable.put(115357243, "SCAFFOLDSUPPORTROPE.OB3");
        modelHashTable.put(-118417362, "BARRIER1.OB3");
        modelHashTable.put(1293420026, "CATABOWARROW.OB3");
        modelHashTable.put(2078550791, "SHIPFRONT.OB3");
        modelHashTable.put(711280256, "TOPLESSTREE.OB3");
        modelHashTable.put(-2113823001, "BIGBED.OB3");
        modelHashTable.put(1467150229, "GRAVESTONE1.OB3");
        modelHashTable.put(-225072515, "TARGET.OB3");
        modelHashTable.put(33852141, "SWORDDUMMY.OB3");
        modelHashTable.put(550912061, "_3663080206");
        modelHashTable.put(-467751691, "FISHINGCRANEROT1.OB3");
        modelHashTable.put(1515407037, "MINECART.OB3");
        modelHashTable.put(1506084138, "CAVE CARVINGS.OB3");
        modelHashTable.put(-912492179, "TRAWLERNET-R.OB3");
        modelHashTable.put(-1920723600, "EGGS.OB3");
        modelHashTable.put(-422344155, "LIGHTNING3.OB3");
        modelHashTable.put(1616754852, "ROCK3.OB3");
        modelHashTable.put(-1564410310, "CUPBOARD.OB3");
        modelHashTable.put(1723886140, "WATERFALL.OB3");
        modelHashTable.put(2138079117, "LANDSCAPE.OB3");
        modelHashTable.put(-959377455, "FIRESPELL1.OB3");
        modelHashTable.put(-1119508409, "CURVEDBONE.OB3");
        modelHashTable.put(-6546792, "CAVE GRILLCAGEUP.OB3");
        modelHashTable.put(-1613885751, "FIREPLACEA3.OB3");
        modelHashTable.put(-174159719, "HENGE.OB3");
        modelHashTable.put(206992721, "SPELLCHARGE1.OB3");
        modelHashTable.put(1065355913, "SARADOMINSTONE.OB3");
        modelHashTable.put(1700129880, "LOGBRIDGE LEV0.OB3");
        modelHashTable.put(1803603598, "LIFTWINCH.OB3");
        modelHashTable.put(-792687148, "SANDYFOOTSTEPS.OB3");
        modelHashTable.put(339872090, "SEWERVALVE.OB3");
        modelHashTable.put(-997847653, "CAVE GRILLTRAPA.OB3");
        modelHashTable.put(-11831722, "MARKET.OB3");
        modelHashTable.put(-903448825, "2-2LIGHT.OB3");
        modelHashTable.put(-1324183500, "JUNGLE STRANGE PLANT 2.OB3");
        modelHashTable.put(-1755754881, "YEWTREE.OB3");
        modelHashTable.put(-1122446273, "TOTEMTREEROTTEN4.OB3");
        modelHashTable.put(-88619105, "1-3LIGHT.OB3");
        modelHashTable.put(-577690439, "CAVE SMALL STAGAMITE.OB3");
        modelHashTable.put(-682783129, "COMPOST.OB3");
        modelHashTable.put(528629423, "JUNGLE FERN TEXTURED 4.OB3");
        modelHashTable.put(2101978215, "BARRELREDCROSS.OB3");
        modelHashTable.put(376704859, "GRAND TREEINSIDE-LEV 1.OB3");
        modelHashTable.put(-995567225, "TRAWLERNET-L.OB3");
        modelHashTable.put(-1285024168, "BARPUMPS.OB3");
        modelHashTable.put(-1483835714, "DOUBLEDOORSCLOSED.OB3");
        modelHashTable.put(180305805, "WOODENGATECLOSED.OB3");
        modelHashTable.put(1223620050, "JUNGLE MEDIUM SIZE PLANT.OB3");
        modelHashTable.put(631907477, "COFFIN2.OB3");
        modelHashTable.put(305822587, "CLAWSPELL2.OB3");
        modelHashTable.put(-1240264876, "CUPBOARDOPEN.OB3");
        modelHashTable.put(-2046677175, "LOGBRIDGEHIGH.OB3");
        modelHashTable.put(1516574510, "SINKINGSHIPFRONT.OB3");
        modelHashTable.put(2142891767, "CLOCKCOG.OB3");
        modelHashTable.put(503499597, "HORNEDSKULL.OB3");
        modelHashTable.put(-587743816, "TOTEMTREE5.OB3");
        modelHashTable.put(-1804936903, "CACTUS.OB3");
        modelHashTable.put(-338475383, "STONESTAND.OB3");
        modelHashTable.put(-892797036, "HILLSIDEDOOR.OB3");
        modelHashTable.put(-1905863932, "CLIMBING_ROCKS.OB3");
        modelHashTable.put(1087788469, "CAVE ROCKTRAP1A.OB3");
        modelHashTable.put(-2089745902, "TELESCOPE.OB3");
        modelHashTable.put(-1271069351, "ROPEFORCLIMBINGBOT.OB3");
        modelHashTable.put(687452562, "BENCH.OB3");
        modelHashTable.put(-1542129281, "SACKS.OB3");
        modelHashTable.put(1934476833, "OBSTICAL_NET.OB3");
        modelHashTable.put(1528455786, "CAVE GRILLTRAP.OB3");
        modelHashTable.put(-1394327148, "DOUBLEDOORFRAME.OB3");
        modelHashTable.put(808698179, "SKULLTORCHA1.OB3");
        modelHashTable.put(1911570563, "CLOCKPOLEBLACK.OB3");
        modelHashTable.put(33525894, "CAVE SNAPTRAP.OB3");
        modelHashTable.put(-1254884274, "BRIDGE SECTION 2.OB3");
        modelHashTable.put(-501483676, "LEVERBRACKET.OB3");
        modelHashTable.put(-1926224719, "GRAND TREE-LEV 3.OB3");
        modelHashTable.put(550294861, "VINE1.OB3");
        modelHashTable.put(-67261825, "GLARIALSSTATUE.OB3");
        modelHashTable.put(-1345038761, "MAGEARENA DOOR.OB3");
        modelHashTable.put(-1648602056, "ZAMORAKSTONE.OB3");
        modelHashTable.put(-1953916401, "GRAND TREE-LEV 1.OB3");
        modelHashTable.put(-643586012, "BLURBERRYBAR.OB3");
        modelHashTable.put(800409149, "CAVE BOLDER.OB3");
        modelHashTable.put(-1243660201, "CHESTOPEN.OB3");
        modelHashTable.put(538488213, "CACTUSWATERED.OB3");
        modelHashTable.put(1126857399, "MAGEARENA CORNERFILL.OB3");
        modelHashTable.put(1371008020, "MILL.OB3");
        modelHashTable.put(1156623701, "SIGNPOST.OB3");
        modelHashTable.put(-463414841, "FIREA2.OB3");
        modelHashTable.put(538300861, "NAILS.OB3");
        modelHashTable.put(660203250, "TRAWLERNET.OB3");
        modelHashTable.put(-1446498202, "LADDERDOWN.OB3");
        modelHashTable.put(146942323, "CLAWSOFIBAN.OB3");
        modelHashTable.put(-277931300, "CAVE TEMPLE.OB3");
        modelHashTable.put(-1951660520, "JUNGLE TREE 1.OB3");
        modelHashTable.put(-342323666, "TREE_WITH_ROPE.OB3");
        modelHashTable.put(1947313574, "CLOCKPOLEBLUE.OB3");
        modelHashTable.put(1451518781, "MAGEARENA TALLCORNER.OB3");
        modelHashTable.put(-1343117748, "LIFTBED.OB3");
        modelHashTable.put(-1260371680, "DOORMAT.OB3");
        modelHashTable.put(823664857, "2-2DARK.OB3");
        modelHashTable.put(-524064510, "STONEDISC.OB3");
        modelHashTable.put(1732888203, "DWARF MULTICANNON PART2.OB3");
        modelHashTable.put(655148751, "OBELISK.OB3");
        modelHashTable.put(109690698, "TORCHA3.OB3");
        modelHashTable.put(-1694371134, "WELL.OB3");
        modelHashTable.put(362241107, "_3914360860");
        modelHashTable.put(-1814968805, "WINDMILLSAIL.OB3");
        modelHashTable.put(-1060436729, "RAILINGS.OB3");
        modelHashTable.put(574140404, "WALLPIPE.OB3");
        modelHashTable.put(-14555407, "SKELETONWITHBAG.OB3");
        modelHashTable.put(-1766659229, "GALLOWS.OB3");
        modelHashTable.put(-1747699195, "LARGESEWERPIPE.OB3");
        modelHashTable.put(-1980954564, "PALM.OB3");
        modelHashTable.put(500937741, "JUNGLE FERN TEXTURED 2.OB3");
        modelHashTable.put(-2055246680, "ANVIL.OB3");
        modelHashTable.put(-1323459388, "GNOMECAGE.OB3");
        modelHashTable.put(587507422, "OGRERELIC.OB3");
        modelHashTable.put(1222538498, "LADDER.OB3");
        modelHashTable.put(-643127180, "TOTEMTREE1.OB3");
        modelHashTable.put(-375494530, "WATCHTOWER.OB3");
        modelHashTable.put(-479223156, "CAVE ROCKTRAP1.OB3");
        modelHashTable.put(708751320, "WALLGRILL.OB3");
        modelHashTable.put(-1552294256, "GNOMEWATCHTOWER LEV1.OB3");
        modelHashTable.put(-2002818699, "CAVE ROCK1.OB3");
        modelHashTable.put(-223380667, "CHAIR.OB3");
        modelHashTable.put(2128129842, "ORNAMENTTREE.OB3");
        modelHashTable.put(-1695175912, "_1512310831");
        modelHashTable.put(208434561, "ENT.OB3");
        modelHashTable.put(1081821589, "TREE_WITH_VINES.OB3");
        modelHashTable.put(-1796293376, "TREEROOT2.OB3");
        modelHashTable.put(-1987551129, "WALLSHIELD.OB3");
        modelHashTable.put(886271265, "CAVE PILLAR.OB3");
        modelHashTable.put(-2038138000, "CAVE TEMPLE ALTER.OB3");
        modelHashTable.put(598885959, "TOTEMTREEEVIL.OB3");
        modelHashTable.put(-479341930, "SPELLSHOCK.OB3");
        modelHashTable.put(333514269, "CLAWSPELL4.OB3");
        modelHashTable.put(-1641577433, "FIREPLACEA1.OB3");
        modelHashTable.put(547303068, "STRAIGHT_LEDGE.OB3");
        modelHashTable.put(-931685773, "FIRESPELL3.OB3");
        modelHashTable.put(969054150, "WALLBENCH.OB3");
        modelHashTable.put(504790033, "_3179009027");
        modelHashTable.put(-909302843, "BRIDGE SECTION CORNER.OB3");
        modelHashTable.put(-1967762242, "GRAND TREE-LEV 0.OB3");
        modelHashTable.put(-1263331649, "POWERINGCRYSTAL.OB3");
        modelHashTable.put(-741690938, "TEST.OB3");
        modelHashTable.put(1581776967, "COPPERROCK1.OB3");
        modelHashTable.put(234684403, "SPELLCHARGE3.OB3");
        modelHashTable.put(603163110, "JUNGLE FERN.OB3");
        modelHashTable.put(1503458180, "WOODENRAILING.OB3");
        modelHashTable.put(-947915419, "COALROCK1.OB3");
        modelHashTable.put(213037743, "LOGBRIDGEJUNCTION LEV1.OB3");
        modelHashTable.put(-1996547359, "DEADTREE2BASE.OB3");
        modelHashTable.put(-1036856301, "ROCKTILE.OB3");
        modelHashTable.put(-926091538, "DWARF MULTICANNON.OB3");
        modelHashTable.put(-1103527909, "MAGEARENA PLAIN WALL.OB3");
        modelHashTable.put(-1912877089, "BAXTORIANCHALICE.OB3");
        modelHashTable.put(586248404, "MUDPATCH.OB3");
        modelHashTable.put(1163500403, "ROWBOATSINKING.OB3");
        modelHashTable.put(-449569000, "FIREA3.OB3");
        modelHashTable.put(296798080, "DUGUPSOIL2.OB3");
        modelHashTable.put(-458479971, "ROCK CAKE COUNTER.OB3");
        modelHashTable.put(-2056930476, "SMALL CAVEENTRANCE2.OB3");
        modelHashTable.put(-1226503977, "CATABOW.OB3");
        modelHashTable.put(-72348283, "HAZEELTOMB.OB3");
        modelHashTable.put(1497433975, "GNOMEGLIDER.OB3");
        modelHashTable.put(-463911314, "FOUNTAIN.OB3");
        modelHashTable.put(-159649725, "GNOMEFENCE2.OB3");
        modelHashTable.put(-1759913695, "HALFBURIEDSKELETON.OB3");
        modelHashTable.put(1428390687, "ARDOUNGEWALL.OB3");
        modelHashTable.put(870261219, "CAVE SWAMPROCKS.OB3");
        modelHashTable.put(836389861, "SKULLTORCHA3.OB3");
        modelHashTable.put(2105499452, "CAVE TEMPLEDOOR.OB3");
        modelHashTable.put(1883223670, "ELVENTOMB.OB3");
        modelHashTable.put(-199187854, "GNOMEHAMEK.OB3");
        modelHashTable.put(30715776, "TRACKCURVE.OB3");
        modelHashTable.put(310643921, "DUGUPSOIL3.OB3");
        modelHashTable.put(843183124, "OBSTICAL_PIPE.OB3");
        modelHashTable.put(-1565123350, "HOLE.OB3");
        modelHashTable.put(604133985, "WOODENGATEOPEN.OB3");
        modelHashTable.put(-1876923827, "TINROCK1.OB3");
        modelHashTable.put(-1177612384, "CAVE SWAMPBUBBLES.OB3");
        modelHashTable.put(-2051527736, "ROWBOAT.OB3");
        modelHashTable.put(2061524112, "WOODENSTAIRS.OB3");
        modelHashTable.put(-693213985, "CAVE BRIDGE SUPPORT.OB3");
        modelHashTable.put(1453382865, "TRACKBUFFER.OB3");
        modelHashTable.put(-323933499, "CAVE TUBETRAPA.OB3");
        modelHashTable.put(1935996178, "POTTERYWHEEL.OB3");
        modelHashTable.put(744370727, "MARBLEARCH.OB3");
        modelHashTable.put(846098451, "WATERPOOL.OB3");
        modelHashTable.put(-563315693, "JUNGLE FERN TEXTURED.OB3");
        modelHashTable.put(407651891, "SMASHEDTABLE.OB3");
        modelHashTable.put(199191902, "LOGBRIDGEJUNCTION LEV0.OB3");
        modelHashTable.put(1660083319, "CAVEENTRANCE2.OB3");
        modelHashTable.put(-945531614, "FIRESPELL2.OB3");
        modelHashTable.put(1301479192, "LARGETREEPLATFORMLOW.OB3");
        modelHashTable.put(301406075, "TREEPLATFORMHIGH.OB3");
        modelHashTable.put(-1704747795, "BOOKCASE.OB3");
        modelHashTable.put(-779569402, "CAULDRON.OB3");
        modelHashTable.put(72266462, "CAVE EXTRA LARGE STAGATITE.OB3");
        modelHashTable.put(-1287376427, "RUNITEROCK1.OB3");
        modelHashTable.put(-1968204739, "BLUEFLOWER.OB3");
        modelHashTable.put(-1261992363, "FOURWAYPLATFORM-LEV 1.OB3");
        modelHashTable.put(1627401113, "ROCKCOUNTER.OB3");
        modelHashTable.put(-1959308884, "METALGATEOPEN.OB3");
        modelHashTable.put(-1268730115, "BRIDGE SECTION 1.OB3");
        modelHashTable.put(-806064790, "ROUNDTABLE.OB3");
        modelHashTable.put(-1653270550, "CLOCKPOLEPURPLE.OB3");
        modelHashTable.put(-1976905586, "LARGETREEPLATFORMHIGH.OB3");
        modelHashTable.put(-214102629, "MAGEARENA WALL.OB3");
        modelHashTable.put(-1136292114, "TOTEMTREEROTTEN3.OB3");
        modelHashTable.put(-862132688, "TRIBALSTATURE.OB3");
        modelHashTable.put(-450035837, "LIGHTNING1.OB3");
        modelHashTable.put(1129172477, "ROCKS1.OB3");
        modelHashTable.put(-2105300943, "CAVE WELL.OB3");
        modelHashTable.put(-1010902771, "BLURITEROCK1.OB3");
        modelHashTable.put(76054075, "CAVE WALLGRILL.OB3");
        modelHashTable.put(1046410473, "CAVE FURNACE.OB3");
        modelHashTable.put(-453905850, "FISHINGCRANEROT2.OB3");
        modelHashTable.put(1641927525, "CAVE OLD BRIDGEDOWN.OB3");
        modelHashTable.put(268164418, "SMALLFERN.OB3");
        modelHashTable.put(1201374497, "BROWNCLIMBINGROCKS.OB3");
        modelHashTable.put(780780595, "CHESTCLOSED.OB3");
        modelHashTable.put(-2081383093, "PORTRAIT.OB3");
        modelHashTable.put(-1390878759, "BUSH1.OB3");
        modelHashTable.put(-1360917469, "SILVERROCK1.OB3");
        modelHashTable.put(401471785, "FLAX.OB3");
        modelHashTable.put(1350529717, "GNOMESIGN.OB3");
        modelHashTable.put(-895876015, "CAVE OLD BRIDGE.OB3");
        modelHashTable.put(1129106817, "BRIDGE SECTION COLLAPSED2.OB3");
        modelHashTable.put(-651461660, "CANDLES.OB3");
        modelHashTable.put(-436189996, "LIGHTNING2.OB3");
        modelHashTable.put(-2049059062, "ALTAR.OB3");
        modelHashTable.put(-2122382425, "MITHRILROCK1.OB3");
        modelHashTable.put(1836511148, "LEVERDOWN.OB3");
        modelHashTable.put(169156853, "SCAFFOLDSUPPORT.OB3");
        modelHashTable.put(167638808, "TREE2.OB3");
        modelHashTable.put(1989138536, "ROPELADDER.OB3");
        modelHashTable.put(575598758, "TABLE.OB3");
        modelHashTable.put(1821534841, "_1721021053");
        modelHashTable.put(51513854, "BEEHIVE.OB3");
        modelHashTable.put(-302620903, "SINKINGBARREL.OB3");
        modelHashTable.put(-1150137955, "TOTEMTREEROTTEN2.OB3");
        modelHashTable.put(-1275838204, "FOURWAYPLATFORM-LEV 0.OB3");
        modelHashTable.put(282952239, "DUGUPSOIL1.OB3");
        modelHashTable.put(1886073496, "BIGROUNDTABLE.OB3");
        modelHashTable.put(252675579, "BROKENPILLAR.OB3");
        modelHashTable.put(321094101, "VINECORNER.OB3");
        modelHashTable.put(-529967472, "MAGEARENA CORNER.OB3");
        modelHashTable.put(504646895, "CORNER_LEDGE.OB3");
        modelHashTable.put(-739728105, "_1657318544");
        modelHashTable.put(-550637102, "VINEJUNCTION.OB3");
        modelHashTable.put(-1146182481, "FISHINGCRANE.OB3");
        modelHashTable.put(-10571683, "SHOPSIGN.OB3");
        modelHashTable.put(-1456352712, "GUTHIXSTONE.OB3");
        modelHashTable.put(-110344601, "CAVE PLATFORM SMALL.OB3");
        modelHashTable.put(-304210222, "DEADTREE2.OB3");
        modelHashTable.put(1164049005, "DOLMEN.OB3");
        modelHashTable.put(-450288091, "POTTERYOVEN.OB3");
        modelHashTable.put(945708879, "CAVE GRILLCAGE.OB3");
        modelHashTable.put(-1937814679, "JUNGLE TREE 2.OB3");
        modelHashTable.put(994192468, "VINE.OB3");
        modelHashTable.put(833447605, "LOGRAFT.OB3");
        modelHashTable.put(-213136302, "IRONROCK1.OB3");
        modelHashTable.put(1195844700, "TRACKPOINTS.OB3");
        modelHashTable.put(-1299154548, "BIGTABLE.OB3");
        modelHashTable.put(-2143185056, "DANGERSIGN.OB3");
        modelHashTable.put(619939794, "PINETREE.OB3");
        modelHashTable.put(786502082, "CAVE PLANKS.OB3");
        modelHashTable.put(-200328404, "CAVE SMALL STAGATITE.OB3");
        modelHashTable.put(1806793877, "SHIPLEAK2.OB3");
        modelHashTable.put(1033168561, "BED.OB3");
        modelHashTable.put(1970164299, "LOG_BALANCE2.OB3");
        modelHashTable.put(-2103659077, "SHIPBACK.OB3");
        modelHashTable.put(-377253206, "CAVE LARGE STAGATITE.OB3");
        modelHashTable.put(-704013022, "LOGPILE.OB3");
        modelHashTable.put(731754672, "METALGATECLOSED.OB3");
        modelHashTable.put(1870880941, "FERN.OB3");
        modelHashTable.put(-754615241, "CAVE LARGE STAGAMITE.OB3");
        modelHashTable.put(731287717, "JUNGLE STATUE.OB3");
        modelHashTable.put(1058027555, "TREESTUMP.OB3");
        modelHashTable.put(2083231518, "FISHING.OB3");
        modelHashTable.put(1520315817, "TRACKSTRAIGHT.OB3");
        modelHashTable.put(-282820580, "_1526156672");
        modelHashTable.put(-1940070560, "GRAND TREE-LEV 2.OB3");
        modelHashTable.put(2080095430, "LONGTABLE.OB3");
        modelHashTable.put(-2077715690, "CAVE LEDGE.OB3");
        modelHashTable.put(564140702, "VINE2.OB3");
        modelHashTable.put(2131015744, "MOSSYROCK.OB3");
        modelHashTable.put(1255595408, "CART.OB3");
        modelHashTable.put(1719042362, "DWARF MULTICANNON PART1.OB3");
        modelHashTable.put(691346041, "WALLCLOCKFACE.OB3");
        modelHashTable.put(291976746, "CLAWSPELL1.OB3");
        modelHashTable.put(-990892099, "STONESTAIRSDOWN.OB3");
        modelHashTable.put(-67013806, "RAMS SKULL DOOROPEN.OB3");
        modelHashTable.put(48447920, "ZODIAC.OB3");
        modelHashTable.put(1992458646, "MUDPILE.OB3");
        modelHashTable.put(-1622051831, "MAPLETREE.OB3");
        modelHashTable.put(428008158, "LARGEURN.OB3");
        modelHashTable.put(1048077592, "ROCKSTEPS.OB3");
        modelHashTable.put(301974171, "SINK.OB3");
        modelHashTable.put(-1994257849, "CAVE SNAPTRAPA.OB3");
        modelHashTable.put(72355642, "MUDPILEDOWN.OB3");
        modelHashTable.put(-1153849262, "LOGBRIDGECURVEDHIGH.OB3");
        modelHashTable.put(1143018318, "ROCKS2.OB3");
        modelHashTable.put(67389303, "HALFBURIEDSKELETON2.OB3");
        modelHashTable.put(-2052812073, "JUNGLE SPIKEY FERN.OB3");
        modelHashTable.put(-207842071, "BAXTORIANCHALICELOW.OB3");
        modelHashTable.put(-1238880931, "POORBED.OB3");
        modelHashTable.put(-417979296, "CAVE GRILLTRAPA UP.OB3");
        modelHashTable.put(-714133991, "POTATO.OB3");
        modelHashTable.put(574285122, "JUNGLE STRANGE PLANT.OB3");
        modelHashTable.put(1253855686, "SPIKEDPIT-LOW.OB3");
        modelHashTable.put(-1434577661, "KHAZARDWALL.OB3");
        modelHashTable.put(1713975721, "LOGBRIDGE LEV1.OB3");
        modelHashTable.put(-640304556, "WOODENSTAIRSDOWN.OB3");
        modelHashTable.put(-1627731592, "FIREPLACEA2.OB3");
        modelHashTable.put(310232574, "CAVE BRIDGE SUPPORTBASE.OB3");
        modelHashTable.put(-30945886, "THRONE.OB3");
        modelHashTable.put(500619870, "FURNACE.OB3");
        modelHashTable.put(-1028377282, "BARREL.OB3");
        modelHashTable.put(220838562, "SPELLCHARGE2.OB3");
        modelHashTable.put(1793200495, "LOGBRIDGELOW.OB3");
        modelHashTable.put(822544020, "SKULLTORCHA2.OB3");
        modelHashTable.put(1633159884, "CAVE BRIDGE STAIRS.OB3");
        modelHashTable.put(-145093425, "CLAYROCK1.OB3");
        modelHashTable.put(-746378213, "_2864382267");
        modelHashTable.put(-1241038433, "BRIDGE SECTION 3.OB3");
        modelHashTable.put(-1566140097, "GNOMEWATCHTOWER LEV0.OB3");
        modelHashTable.put(-117209447, "NASTYFUNGUS.OB3");
        modelHashTable.put(70179765, "DRAMENTREE.OB3");
        modelHashTable.put(850235702, "SKULLTORCHA4.OB3");
        modelHashTable.put(789184662, "METALGATEOPEN2.OB3");
        modelHashTable.put(-147271527, "TREE_FOR_ROPE.OB3");
        modelHashTable.put(2045966935, "BROKENWALL.OB3");
        modelHashTable.put(2081280933, "JUNGLE FLY TRAP.OB3");
        modelHashTable.put(1259920560, "OBSTICAL_ROPESWING.OB3");
        modelHashTable.put(362859018, "GRAND TREEINSIDE-LEV 0.OB3");
        modelHashTable.put(-1538448415, "GNOMEWATCHTOWER LEV2.OB3");
        modelHashTable.put(190129401, "LEVERUP.OB3");
        modelHashTable.put(450577783, "WHEAT.OB3");
        modelHashTable.put(1037362809, "ROCKCAKE.OB3");
        modelHashTable.put(-1810139217, "TREEROOT1.OB3");
        modelHashTable.put(1384607026, "BROKENLOGRAFT.OB3");
        modelHashTable.put(624033417, "BROKENCART.OB3");
        modelHashTable.put(-629281339, "TOTEMTREE2.OB3");
        modelHashTable.put(-605711323, "OBSTICAL_FRAME.OB3");
        modelHashTable.put(319668428, "CLAWSPELL3.OB3");
        modelHashTable.put(1746734044, "DWARF MULTICANNON PART3.OB3");
        modelHashTable.put(1215171501, "SPEARWALL.OB3");
        modelHashTable.put(-1627606728, "MUSHROOM.OB3");
        modelHashTable.put(-440792449, "BIGEGG.OB3");
        modelHashTable.put(123536539, "TORCHA4.OB3");
        modelHashTable.put(-1405472792, "MANHOLECLOSED.OB3");
        modelHashTable.put(692104855, "STOOL.OB3");
        modelHashTable.put(347360110, "CLAWSPELL5.OB3");
        modelHashTable.put(332407820, "CHAOSALTAR.OB3");
        modelHashTable.put(2033834712, "RAMS SKULL DOOR.OB3");
        modelHashTable.put(1449903497, "CRATE.OB3");
        modelHashTable.put(1735304330, "METALGATECLOSED2.OB3");
        modelHashTable.put(390550700, "GRAND TREEINSIDE-LEV 2.OB3");
        modelHashTable.put(823872246, "CAVE TEMPLEDOOROPEN.OB3");
        modelHashTable.put(-135276543, "SHIPMIDDLE.OB3");
        modelHashTable.put(1882511323, "SIGNPOST2.OB3");
        modelHashTable.put(-138856362, "TREE.OB3");
        modelHashTable.put(514783582, "JUNGLE FERN TEXTURED 3.OB3");
        modelHashTable.put(946920320, "SUPPORTNW.OB3");
        modelHashTable.put(-1673355341, "XMASTREE.OB3");
        modelHashTable.put(-76785506, "LARGEGRAVE.OB3");
        modelHashTable.put(-619279131, "1-1LIGHT.OB3");
        modelHashTable.put(781731179, "BEAM.OB3");
        modelHashTable.put(57619607, "CEILINGWEB.OB3");
        modelHashTable.put(353417961, "_1347842162");
        modelHashTable.put(-477260682, "FIREA1.OB3");
        modelHashTable.put(1118659921, "1-3DARK.OB3");
        modelHashTable.put(978704810, "2-1LIGHT.OB3");
        modelHashTable.put(1039900591, "MYSTERIOUS RUINS.OB3");
        modelHashTable.put(-1937080672, "PORTAL.OB3");
        modelHashTable.put(-805897215, "ESSENCEMINE.OB3");
        modelHashTable.put(-1044363307, "EXHAUSTEDTREE.OB3");
        modelHashTable.put(1412142961, "LEMONTREE.OB3");
        modelHashTable.put(405124961, "LIMETREE.OB3");
        modelHashTable.put(-620269496, "APPLETREE.OB3");
        modelHashTable.put(632562334, "ORANGETREE.OB3");
        modelHashTable.put(-1116097073, "GRAPEFRUITTREE.OB3");
        modelHashTable.put(1408505787, "EXHAUSTEDPALM.OB3");
        modelHashTable.put(67449385, "EXHAUSTEDPALM2.OB3");
        modelHashTable.put(789677037, "BANANAPALM.OB3");
        modelHashTable.put(781115500, "PAPAYAPALM.OB3");
        modelHashTable.put(-1070805829, "COCONUTPALM.OB3");
        modelHashTable.put(864902172, "DEPLETEDPLANT.OB3");
        modelHashTable.put(1513846319, "PINEAPPLEPLANT.OB3");
        modelHashTable.put(45241727, "DEPLETEDBUSH.OB3");
        modelHashTable.put(1351719227, "REDBERRYBUSH.OB3");
        modelHashTable.put(219352724, "CADAVABERRYBUSH.OB3");
        modelHashTable.put(-820757332, "DWELLBERRYBUSH.OB3");
        modelHashTable.put(-818530481, "JANGERBERRYBUSH.OB3");
        modelHashTable.put(625826885, "WHITEBERRYBUSH.OB3");
        modelHashTable.put(1890286552, "GREENCABBAGE.OB3");
        modelHashTable.put(-1829945830, "REDCABBAGE.OB3");
        modelHashTable.put(-1147154594, "PUMPKIN.OB3");
        modelHashTable.put(-706058788, "POTATOPLANT.OB3");
        modelHashTable.put(1415266948, "ONIONPLANT.OB3");
        modelHashTable.put(-1855212389, "GARLICPLANT.OB3");
        modelHashTable.put(1834707905, "DEPLETEDTOMATO.OB3");
        modelHashTable.put(-1774105033, "DEPLETEDCORN.OB3");
        modelHashTable.put(-716276439, "TOMATOPLANT.OB3");
        modelHashTable.put(1385630527, "CORNPLANT.OB3");
        modelHashTable.put(-1682324551, "SNAPEGRASS.OB3");
        modelHashTable.put(-641718453, "HERB.OB3");
        modelHashTable.put(-249761852, "SOILMOUND.OB3");
        modelHashTable.put(-93780729, "PUMPKINWHITE.OB3");
        modelHashTable.put(-206398111, "BARRELWATER.OB3");
        modelHashTable.put(-189576848, "COMPOSTBIN.OB3");
        modelHashTable.put(1368656458, "COMPOSTBIN2.OB3");
        modelHashTable.put(-856328836, "SEAWEED.OB3");
        modelHashTable.put(-1233484250, "LIMPWURTROOT.OB3");
        modelHashTable.put(-2087359209, "SUGARCANE.OB3");
        modelHashTable.put(1110360451, "GRAPEVINE.OB3");
        modelHashTable.put(-1868560273, "DRAGONFRUIT.OB3");
        modelHashTable.put(-684909398, "DEPLETEDDRAGONFRUIT.OB3");
    }

    public static InputStream streamFromPath(String path) throws IOException {
        return new BufferedInputStream(new FileInputStream(path));
    }

    public static int getModelFileSize(RSModel model) {
        if (model == null)
            return 0;
        //init to 4 from the vertex/face count headers
        int fileSize = 4;

        //6 bytes for each vertex
        fileSize += model.getVertexCount() * 6;

        //1 byte for each face index count
        fileSize += model.getFaceCount();

        //4 bytes for each face for textures
        fileSize += model.getFaceCount() * 4;

        //1 byte for each face for diffuse light
        fileSize += model.getFaceCount();

        //
        int countIndices = 0;

        for (int i = 0; i < model.getFaceCount(); i++)
            countIndices += model.getFacePointCount(i);

        if (model.getVertexCount() < 256)
            fileSize += countIndices;
        else
            fileSize += 2 * countIndices;

        return fileSize;
    }

    public static void readFromPath(String path, byte abyte0[], int length) throws IOException {
        InputStream inputstream = streamFromPath(path);
        DataInputStream datainputstream = new DataInputStream(inputstream);
        try {
            datainputstream.readFully(abyte0, 0, length);
        } catch (EOFException _ex) {
            _ex.printStackTrace();
        }
        datainputstream.close();
    }

    public static int getUnsignedByte(byte byte0) {
        return byte0 & 0xff;
    }

    public static int getUnsigned2Bytes(byte abyte0[], int i) {
        return ((abyte0[i] & 0xff) << 8) + (abyte0[i + 1] & 0xff);
    }

    public static int getUnsigned4Bytes(byte abyte0[], int i) {
        return ((abyte0[i] & 0xff) << 24) + ((abyte0[i + 1] & 0xff) << 16) + ((abyte0[i + 2] & 0xff) << 8)
                + (abyte0[i + 3] & 0xff);
    }

    public static long getUnsigned8Bytes(byte abyte0[], int i) {
        return (((long) getUnsigned4Bytes(abyte0, i) & 0xffffffffL) << 32)
                + ((long) getUnsigned4Bytes(abyte0, i + 4) & 0xffffffffL);
    }

    public static int readInt(byte abyte0[], int i) {
        return ((abyte0[i] & 0xff) << 24) | ((abyte0[i + 1] & 0xff) << 16) | ((abyte0[i + 2] & 0xff) << 8)
                | (abyte0[i + 3] & 0xff);
    }

    public static int getShort(byte abyte0[], int i) {
        int j = getUnsignedByte(abyte0[i]) * 256 + getUnsignedByte(abyte0[i + 1]);
        if (j > 32767) {
            j -= 0x10000;
        }
        return j;
    }

    public static int getSigned4Bytes(byte abyte0[], int i) {
        if ((abyte0[i] & 0xff) < 128) {
            return abyte0[i];
        } else {
            return ((abyte0[i] & 0xff) - 128 << 24) + ((abyte0[i + 1] & 0xff) << 16) + ((abyte0[i + 2] & 0xff) << 8)
                    + (abyte0[i + 3] & 0xff);
        }
    }

    public static int getIntFromByteArray(byte byteArray[], int offset, int length) {
        int bitOffset = offset >> 3;
        int bitMod = 8 - (offset & 7);
        int i1 = 0;
        for (; length > bitMod; bitMod = 8) {
            i1 += (byteArray[bitOffset++] & baseLengthArray[bitMod]) << length - bitMod;
            length -= bitMod;
        }

        if (length == bitMod)
            i1 += byteArray[bitOffset] & baseLengthArray[bitMod];
        else
            i1 += byteArray[bitOffset] >> bitMod - length & baseLengthArray[length];
        return i1;
    }

    public static String addCharacters(String s, int i) {
        String s1 = "";
        for (int j = 0; j < i; j++)
            if (j >= s.length()) {
                s1 = s1 + " ";
            } else {
                char c = s.charAt(j);
                if (c >= 'a' && c <= 'z')
                    s1 = s1 + c;
                else if (c >= 'A' && c <= 'Z')
                    s1 = s1 + c;
                else if (c >= '0' && c <= '9')
                    s1 = s1 + c;
                else
                    s1 = s1 + '_';
            }

        return s1;
    }

    public static long stringLength12ToLong(String s) {
        String s1 = "";
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= 'a' && c <= 'z')
                s1 = s1 + c;
            else if (c >= 'A' && c <= 'Z')
                s1 = s1 + (char) ((c + 97) - 65);
            else if (c >= '0' && c <= '9')
                s1 = s1 + c;
            else
                s1 = s1 + ' ';
        }

        s1 = s1.trim();
        if (s1.length() > 12)
            s1 = s1.substring(0, 12);
        long l = 0L;
        for (int j = 0; j < s1.length(); j++) {
            char c1 = s1.charAt(j);
            l *= 37L;
            if (c1 >= 'a' && c1 <= 'z')
                l += (1 + c1) - 97;
            else if (c1 >= '0' && c1 <= '9')
                l += (27 + c1) - 48;
        }

        return l;
    }

    public static long nameToHash(String arg0) {
        String s = "";
        for (int i = 0; i < arg0.length(); i++) {
            char c = arg0.charAt(i);
            if (c >= 'a' && c <= 'z')
                s = s + c;
            else if (c >= 'A' && c <= 'Z')
                s = s + (char) ((c + 97) - 65);
            else if (c >= '0' && c <= '9')
                s = s + c;
            else
                s = s + ' ';
        }

        s = s.trim();
        if (s.length() > 12)
            s = s.substring(0, 12);
        long l = 0L;
        for (int j = 0; j < s.length(); j++) {
            char c1 = s.charAt(j);
            l *= 37L;
            if (c1 >= 'a' && c1 <= 'z')
                l += (1 + c1) - 97;
            else if (c1 >= '0' && c1 <= '9')
                l += (27 + c1) - 48;
        }

        return l;
    }

    public static String longToString(long l) {
        if (l < 0L)
            return "invalid_name";
        String s = "";
        while (l != 0L) {
            int i = (int) (l % 37L);
            l /= 37L;
            if (i == 0)
                s = " " + s;
            else if (i < 27) {
                if (l % 37L == 0L)
                    s = (char) ((i + 65) - 1) + s;
                else
                    s = (char) ((i + 97) - 1) + s;
            } else {
                s = (char) ((i + 48) - 27) + s;
            }
        }
        return s;
    }

    public static int getDataFileOffset(String filename, byte data[]) {
        int numEntries = getUnsigned2Bytes(data, 0);
        int wantedHash = 0;
        filename = filename.toUpperCase();
        for (int k = 0; k < filename.length(); k++)
            wantedHash = (wantedHash * 61 + filename.charAt(k)) - 32;

        int offset = 2 + numEntries * 10;
        for (int entry = 0; entry < numEntries; entry++) {
            int fileHash = (data[entry * 10 + 2] & 0xff) * 0x1000000 + (data[entry * 10 + 3] & 0xff) * 0x10000
                    + (data[entry * 10 + 4] & 0xff) * 256 + (data[entry * 10 + 5] & 0xff);
            int fileSize = (data[entry * 10 + 9] & 0xff) * 0x10000 + (data[entry * 10 + 10] & 0xff) * 256
                    + (data[entry * 10 + 11] & 0xff);
            System.out.println("fileHashes.add(" + fileHash + ");");

            if (fileHash == wantedHash)
                return offset;
            offset += fileSize;
        }

        return 0;
    }

    public static Unpacker.entryInfo getIndexOffset(int index, byte data[]) {
        Unpacker.entryInfo info = new Unpacker.entryInfo(0, 0);
        int numEntries = getUnsigned2Bytes(data, 0);
        info.offset = 2 + numEntries * 10;
        for (int entry = 0; entry < numEntries; entry++) {
            int fileSize = (data[entry * 10 + 9] & 0xff) * 0x10000 + (data[entry * 10 + 10] & 0xff) * 256
                    + (data[entry * 10 + 11] & 0xff);
            if (entry == index) {
                info.hash = (data[entry * 10 + 2] & 0xff) * 0x1000000 + (data[entry * 10 + 3] & 0xff) * 0x10000
                        + (data[entry * 10 + 4] & 0xff) * 256 + (data[entry * 10 + 5] & 0xff);
                return info;
            }

            info.offset += fileSize;
        }
        return info;
    }

    public static int getDataFileLength(String filename, byte data[]) {
        int numEntries = getUnsigned2Bytes(data, 0);
        int wantedHash = 0;
        filename = filename.toUpperCase();
        for (int k = 0; k < filename.length(); k++)
            wantedHash = (wantedHash * 61 + filename.charAt(k)) - 32;

        int offset = 2 + numEntries * 10;
        for (int i1 = 0; i1 < numEntries; i1++) {
            int fileHash = (data[i1 * 10 + 2] & 0xff) * 0x1000000 + (data[i1 * 10 + 3] & 0xff) * 0x10000
                    + (data[i1 * 10 + 4] & 0xff) * 256 + (data[i1 * 10 + 5] & 0xff);
            int fileSize = (data[i1 * 10 + 6] & 0xff) * 0x10000 + (data[i1 * 10 + 7] & 0xff) * 256
                    + (data[i1 * 10 + 8] & 0xff);
            int fileSizeCompressed = (data[i1 * 10 + 9] & 0xff) * 0x10000 + (data[i1 * 10 + 10] & 0xff) * 256
                    + (data[i1 * 10 + 11] & 0xff);
            if (fileHash == wantedHash)
                return fileSize;
            offset += fileSizeCompressed;
        }

        return 0;
    }

    public static byte[] loadData(String file, int len, byte[] arc) {
        return loadData(file, len, arc, null);
    }

    public static byte[] loadData(String file, int len, byte[] arc, byte[] dest) {
        int arc_length = (arc[0] & 0xff) * 256 + (arc[1] & 0xff);
        int hash = 0;
        file = file.toUpperCase();
        for (int i = 0; i < file.length(); i++)
            hash = hash * 61 + file.charAt(i) - 32;
        int offset = 2 + arc_length * 10;
        for (int i = 0; i < arc_length; i++) {
            int entry_hash = (arc[(i * 10 + 2)] & 0xFF) * 16777216 + (arc[(i * 10 + 3)] & 0xFF) * 65536
                    + (arc[(i * 10 + 4)] & 0xFF) * 256 + (arc[(i * 10 + 5)] & 0xFF);
            int decmp_len = (arc[(i * 10 + 6)] & 0xFF) * 65536 + (arc[(i * 10 + 7)] & 0xFF) * 256
                    + (arc[(i * 10 + 8)] & 0xFF);
            int cmp_len = (arc[(i * 10 + 9)] & 0xFF) * 65536 + (arc[(i * 10 + 10)] & 0xFF) * 256
                    + (arc[(i * 10 + 11)] & 0xFF);

            if (entry_hash == hash) {
                if (dest == null)
                    dest = new byte[decmp_len + len];
                if (decmp_len != cmp_len)
                    DataFileDecrypter.unpackData(dest, decmp_len, arc, cmp_len, offset);
                else {
                    for (int ii = 0; ii < decmp_len; ii++) {
                        dest[ii] = arc[(offset + ii)];
                    }
                }
                return dest;
            }
            offset += cmp_len;
        }
        return null;
    }

    public static String hashLookup(int hash) {
        if (modelHashTable.containsKey(hash)) {
            String ret = modelHashTable.get(hash);
            if (ret.equalsIgnoreCase("unknown"))
                return String.format("%d", hash);
            else
                return modelHashTable.get(hash);
        }

        return String.format("%d", hash);
    }
}
