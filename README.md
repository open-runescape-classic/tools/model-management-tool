The model management tool may be used for importing low poly 3D mesh .OBJ models into the scenery cache in the custom client and for painting or texturing polygon faces. Uses built-in Zulu OpenJDK 15 with OpenFX to launch. Currently windows-only.

Some imported objects will not render. Try to scale them 20x or greater in Blender and re-export and see if they show up. We are still working on how to improve the tool. 

After saving and packing, the output will be saved in "model-management-tool\out\OpenRSCModelTool\res\models.orsc" and you will have to copy this to the client Cache, update how it is referenced in the server and client, and in theory, it should load properly.