set PATH_TO_FX="zulu15.28.51-ca-fx-jdk15.0.1-win_x64\lib"
set PATH_TO_FX_MODS="zulu15.28.51-ca-fx-jdk15.0.1-win_x64"

rmdir "out/mods" /s /q
dir /s /b src\*.java > sources.txt & javac --module-path "%PATH_TO_FX_MODS%;lib" -d out/mods/modeltool @sources.txt & del sources.txt
rmdir "out/OpenRSCModelTool" /s /q
jlink --module-path "%PATH_TO_FX_MODS%;lib;out/mods" --add-modules modeltool --output out/OpenRSCModelTool
copy "properscript" "out/OpenRSCModelTool/RUN.bat"
xcopy /E /I /S "src" "out/OpenRSCModelTool/src" /EXCLUDE:excluded.txt
xcopy /E /I /S "res" "out/OpenRSCModelTool/res"
cd out/OpenRSCModelTool/src
for /f "delims=" %%d in ('dir /s /b /ad ^| sort /r') do rd "%%d"